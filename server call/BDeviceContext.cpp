

#include "BDeviceContext.h"


CBDeviceContext::CBDeviceContext()
{
	//成员变量清零
	m_hDC = NULL;
	m_hWnd = NULL;
	m_hBitmap = NULL; m_hBitmapOld = NULL;
	m_hPen = NULL; m_hPenOld = NULL;
	m_hBrush = NULL; m_hBrushOld = NULL;
}

CBDeviceContext::CBDeviceContext( HWND hWnd )
{
	// 成员变量清零
	m_hDC = NULL;
	m_hWnd = NULL;
	m_hBitmap = NULL; m_hBitmapOld = NULL;
	m_hPen = NULL; m_hPenOld = NULL;
	m_hBrush = NULL; m_hBrushOld = NULL;
	// 通过 SetFromGetDC 设置本对象
	SetFromGetDC(hWnd);
}

CBDeviceContext::CBDeviceContext( HDC hDCCompatible, int iBmpWidth, int iBmpHeight )
{
	// 成员变量清零
	m_hDC = NULL;
	m_hWnd = NULL;
	m_hBitmap = NULL; m_hBitmapOld = NULL;
	m_hPen = NULL; m_hPenOld = NULL;
	m_hBrush = NULL; m_hBrushOld = NULL;
	// 通过 SetFromCreateCompatible 设置本对象
	SetFromCreateCompatible(hDCCompatible, iBmpWidth, iBmpHeight);
}

CBDeviceContext::CBDeviceContext( HDC hDCCompatible, unsigned short int idBmp )
{
	// 成员变量清零
	m_hDC = NULL;
	m_hWnd = NULL;
	m_hBitmap = NULL; m_hBitmapOld = NULL;
	m_hPen = NULL; m_hPenOld = NULL;
	m_hBrush = NULL; m_hBrushOld = NULL;
	// 通过 SetFromBitmap 设置本对象
	SetFromBitmap(hDCCompatible, idBmp);
}

CBDeviceContext::~CBDeviceContext()
{
	// 释放资源
	ReleaseDeviceContext();	// 将自动调用其他 RelaseXXX 函数，先释放其他资源
}


bool CBDeviceContext::SetFromGetDC( HWND hWnd )
{
	ReleaseDeviceContext();	// 将自动调用其他 RelaseXXX 函数，先释放其他资源

	m_hDC = GetDC(hWnd);
	if (NULL==m_hDC) 
		return false;
	else
	{
		m_hWnd = hWnd;			// m_hWnd 不为 NULL 表示 hDC来自 GetDC
		return true;
	}

}

bool CBDeviceContext::SetFromCreateCompatible( HDC hDCCompatible, int iBmpWidth, int iBmpHeight )
{
	ReleaseDeviceContext();	// 将自动调用其他 RelaseXXX 函数，先释放其他资源

	m_hDC = CreateCompatibleDC(hDCCompatible); m_hWnd = NULL;	// m_hWnd == NULL 表示 m_hDC 来自 CreateDC
	if (NULL==m_hDC) return false;
	m_hBitmap = CreateCompatibleBitmap(hDCCompatible, iBmpWidth, iBmpHeight);
	if (NULL==m_hBitmap) return false;
	m_hBitmapOld = (HBITMAP)SelectObject(m_hDC, m_hBitmap);
	return true;
}

bool CBDeviceContext::SetFromBitmap( HDC hDCCompatible, unsigned short int idBmp )
{
	ReleaseDeviceContext();	// 将自动调用其他 RelaseXXX 函数，先释放其他资源

	m_hDC = CreateCompatibleDC(hDCCompatible); m_hWnd = NULL;	// m_hWnd == NULL 表示 m_hDC 来自 CreateDC
	if (NULL==m_hDC) return false;
	m_hBitmap = LoadBitmap(pApp->hInstance, MAKEINTRESOURCE(idBmp));
	if (NULL==m_hBitmap) return false;
	m_hBitmapOld = (HBITMAP)SelectObject(m_hDC, m_hBitmap);
	return true;
}

void CBDeviceContext::ReleaseDeviceContext()
{
	//释放设备环境
	if (m_hDC)
	{
		ReleaseBitmap();
		ReleasePen();
		ReleaseBrush();
		if (m_hWnd)		// m_hWnd 不为 NULL 表示 hDC来自 GetDC；否则表示来自 CreateDC
			ReleaseDC(m_hWnd, m_hDC);
		else
			DeleteDC(m_hDC);
		m_hDC = NULL;
	}
	m_hWnd = NULL;
}

void CBDeviceContext::ReleaseBitmap()
{
	// 恢复位图
	if (m_hBitmapOld)	
	{ SelectObject(m_hDC, m_hBitmapOld); m_hBitmapOld=NULL; }
	// 删除位图
	if (m_hBitmap)
	{ DeleteObject(m_hBitmap);	m_hBitmap=NULL;	}
}

void CBDeviceContext::ReleasePen()
{
	// 恢复画笔
	if (m_hPenOld)
	{ SelectObject(m_hDC, m_hPenOld);	m_hPenOld=NULL; }
	// 删除当前画笔
	if (m_hPen)
	{ DeleteObject(m_hPen); m_hPen=NULL; }
}

void CBDeviceContext::ReleaseBrush()
{
	// 恢复刷子
	if (m_hBrushOld)
	{ SelectObject(m_hDC, m_hBrushOld);	m_hBrushOld=NULL; }
	// 删除当前刷子
	if (m_hBrush)
	{ DeleteObject(m_hBrush); m_hBrush=NULL; }
}

HDC CBDeviceContext::hDC()
{
	return m_hDC;
}

HBITMAP CBDeviceContext::hBitmap()
{
	return m_hBitmap;
}

HPEN CBDeviceContext::hPen()
{
	return m_hPen;
}

HBRUSH CBDeviceContext::hBrush()
{
	return m_hBrush;
}

