
//////////////////////////////////////////////////////////////////////////
// 管理 DC 绘图对象

#include "BWindows.h"

class CBDeviceContext
{
public:
	CBDeviceContext();
	CBDeviceContext(HWND hWnd);
	CBDeviceContext(HDC hDCCompatible, int iBmpWidth, int iBmpHeight);
	CBDeviceContext(HDC hDCCompatible, unsigned short int idBmp);
	~CBDeviceContext();

	// 通过一个窗口/控件句柄，设置本对象：自动 GetDC
	bool SetFromGetDC(HWND hWnd); 

	// 通过建立一个 兼容 DC，设置本对象：自动创建兼容 DC，并自动创建兼容位图
	bool SetFromCreateCompatible(HDC hDCCompatible, int iBmpWidth, int iBmpHeight);

	// 通过一个资源中的位图，设置本对象：自动创建兼容 DC，并自动选入位图；返回成功/失败
	bool SetFromBitmap(HDC hDCCompatible, unsigned short int idBmp);

	// 返回本对象管理的设备环境句柄
	HDC hDC();
	// 返回本对象管理的目前各对象
	HBITMAP hBitmap();
	HPEN hPen();
	HBRUSH hBrush();

private:
	// 释放资源：每个函数分别释放其中对应的资源
	void ReleaseBitmap();
	void ReleasePen();
	void ReleaseBrush();
	void ReleaseDeviceContext();	// 将自动调用其他 RelaseXXX 函数，先释放其他资源

private:
	HDC m_hDC;
	HWND m_hWnd;	// =NULL 表示 m_hDC 来自 CreateDC；否则表示来自 GetDC
	HBITMAP m_hBitmap, m_hBitmapOld;
	HPEN m_hPen, m_hPenOld;
	HBRUSH m_hBrush, m_hBrushOld;
};