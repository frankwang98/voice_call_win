#include "BList.h"


CBList::CBList()
{
	m_ListHead = NULL;
	m_ilistLen = 0;
}

CBList::~CBList()
{
	ListClear();
}

void CBList::ListClear()
{
	//清空函数，和析构一样
	NODE *pTemp = m_ListHead;
	while (m_ListHead)
	{
		pTemp = m_ListHead;
		m_ListHead = m_ListHead->next;
		//注意，如果此处的指针不为空，那么应该释放指针指向位置的地址空间
		if(pTemp->pDataElse)	
			delete pTemp->pDataElse;
		delete(pTemp);
	}
}

bool CBList::IsEmpty()
{
	return m_ListHead == NULL;
}

//获取链表长度
int CBList::GetListLength()
{
// 	NODE *p = head;
// 	int len = 0;
// 	while (p != NULL)
// 	{
// 		len++;
// 		p = p->next;
// 	}
// 	return len;
	return m_ilistLen;
}

bool CBList::GetElemFromIndex(int i, int* e)	//*e是返回的元素
{
	//如果超出链表长度，直接返回失败
	if (i >= m_ilistLen)
		return false;

    NODE *pTemp = m_ListHead;
    int j = 0;

	//仅仅当链表指针为真，并且当前的检索位置小于i的时候向后进行检索
    while (pTemp && j<i)
    {
        pTemp = pTemp->next;
        j++;
    }

	//如果是空的，那么就是检索失败，即位置超出了最长长度
    if (pTemp == NULL) 
		return false;
	else
	{
		*e = pTemp->DataInt;
		return true;
	}
}

int CBList::LocateElem(int iData)
{
	int i = 0;
	NODE *p = m_ListHead;
	//检索链表的数据并比对返回正确的数据下标，从 0 开始
	while (p != NULL)
	{
		if (p->DataInt == iData)
			return i;
		else p = p->next;
		i++;
	}

	return -1;
}

bool CBList::GetPriorElem(int cur_e, int *pre_e)
{
	NODE *p = m_ListHead;

	//是头结点，不存在上一个元素
	if (p->DataInt == cur_e)
		return false;

	while (p->next != NULL)
	{
		//如果当前节点的下一个节点的元素等于这个 cur_e，那么就把当前节点的数据赋值给 *pre_e，返回成功
		if (p->next->DataInt == cur_e)
		{
			*pre_e = p->DataInt;
			return true;
		}
		else
			p = p->next;
	}
	return false;	//遍历完不存在或者只有一个头结点
}

bool CBList::GetNextElem(int cur_e, int *next_e)
{
	NODE *p = m_ListHead;

	//如果头节点为空或者头节点的下一个节点内容是空，那么直接返回失败
	if (m_ListHead == NULL || m_ListHead->next == NULL) 
		return false;

	while (p->next != NULL)
	{
		if (p->DataInt == cur_e)
		{
			*next_e = p->next->DataInt;
			return true;
		}
		else
			p = p->next;
	}
	return false;
}

int CBList::ListInsert( int iDataInsert, int iIndex /*=0*/ )
{
	//如果 i 超出了最终的长度，直接返回失败
	if (iIndex > m_ilistLen)
		return -1;

	NODE *pTemp = m_ListHead;
	NODE* pNew = new NODE;

	//初始化新的节点的内容
	pNew->DataInt = iDataInsert;	//默认插入数据为第一个整数内容
	pNew->DataLong = 0;
	pNew->DataLong2 = 0;
	pNew->DataStr = TEXT("");
	pNew->DataStr2 = TEXT("");
	pNew->pDataElse = NULL;
	pNew->next = NULL;

	//如果插入的节点为头节点，操作不同
	if (iIndex == 0)
	{
		pNew->next = pTemp;
		m_ListHead = pNew;		//直接新添加的当作头节点
		m_ilistLen++;
		return iIndex;
	}
	else
	{
		pTemp = NodeFromIndex(iIndex-1);	//获得插入点前面的节点的指针内容
		pNew->next = pTemp->next;
		pTemp->next = pNew;
		m_ilistLen++;
		return iIndex;
	}
}

bool CBList::ListDelete(int iIndex)
{
	if (iIndex >= m_ilistLen)
		return false;

	NODE* pTemp = NULL;
	NODE* pSelect = NULL;
	if (iIndex == 0)
	{
		pSelect = m_ListHead;
		pTemp = pSelect->next;
		delete pSelect;
		pSelect = NULL;
		m_ListHead = pTemp;
	}
	else
	{
		pTemp = NodeFromIndex(iIndex-1);		//首先找到待删除的节点内容
		pSelect = pTemp->next;
		//删除该节点的内容
		pTemp->next = pSelect->next;

		//注意，如果此处的指针不为空，那么应该释放指针指向位置的地址空间
		if(pSelect->pDataElse)
			delete pSelect->pDataElse;

		delete pSelect;
		pSelect = NULL;
	}
	return true;
}

NODE* CBList::ListReverse()
{
	//如果链表的头部为空或者说下一个内容为空，那么直接返回原来的链表头部
	if (m_ListHead == NULL || m_ListHead->next == NULL) 
		return m_ListHead;
	
	NODE *p = m_ListHead;
	NODE *q = m_ListHead->next;
	NODE *r;
	m_ListHead->next = NULL;
	//懒得追究了，就这样子吧
	while (q)
	{
		r = q->next;
		q->next = p;
		p = q;
		q = r;
	}
	m_ListHead = p;

	return m_ListHead;
}

bool CBList::SetDataInt( int iIndex, int iData )
{
	NODE* pTemp = NodeFromIndex(iIndex);
	if (pTemp)
	{
		pTemp->DataInt = iData;
		return true;
	}
	else
	return false;
}

bool CBList::SetDataInt2( int iIndex, int iData )
{
	NODE* pTemp = NodeFromIndex(iIndex);
	if (pTemp)
	{
		pTemp->DataInt2 = iData;
		return true;
	}
	else
		return false;
}

bool CBList::SetDataLong( int iIndex, long dData )
{
	NODE* pTemp = NodeFromIndex(iIndex);
	if (pTemp)
	{
		pTemp->DataLong = dData;
		return true;
	}
	else
		return false;
}

bool CBList::SetDataLong2( int iIndex, long dData )
{
	NODE* pTemp = NodeFromIndex(iIndex);
	if (pTemp)
	{
		pTemp->DataLong2 = dData;
		return true;
	}
	else
		return false;
}

bool CBList::SetDataStr( int iIndex, LPCTSTR sData )
{
	NODE* pTemp = NodeFromIndex(iIndex);
	if (pTemp)
	{
		pTemp->DataStr = sData;
		return true;
	}
	else
		return false;
}

bool CBList::SetDataStr2( int iIndex, LPCTSTR sData )
{
	NODE* pTemp = NodeFromIndex(iIndex);
	if (pTemp)
	{
		pTemp->DataStr2 = sData;
		return true;
	}
	else
		return false;
}

bool CBList::SetDatapVoid( int iIndex, LPVOID pData )
{
	NODE* pTemp = NodeFromIndex(iIndex);
	if (pTemp)
	{
		pTemp->pDataElse = pData;
		return true;
	}
	else
		return false;
}

NODE* CBList::GetListHead()
{
	return m_ListHead;
}

int CBList::GetDataInt( int iIndex )
{
	NODE* pTemp = NodeFromIndex(iIndex);
	if (pTemp)
		return pTemp->DataInt;
	else
		return 0;
}

int CBList::GetDataInt2( int iIndex )
{
	NODE* pTemp = NodeFromIndex(iIndex);
	if (pTemp)
		return pTemp->DataInt2;
	else
		return 0;
}

long CBList::GetDataLong( int iIndex )
{
	NODE* pTemp = NodeFromIndex(iIndex);
	if (pTemp)
		return pTemp->DataLong;
	else
		return 0;
}

long CBList::GetDataLong2( int iIndex )
{
	NODE* pTemp = NodeFromIndex(iIndex);
	if (pTemp)
		return pTemp->DataLong2;
	else
		return 0;
}

tstring CBList::GetDataStr( int iIndex )
{
	NODE* pTemp = NodeFromIndex(iIndex);
	if (pTemp)
		return pTemp->DataStr;
	else
		return TEXT("");
}

tstring CBList::GetDataStr2( int iIndex )
{
	NODE* pTemp = NodeFromIndex(iIndex);
	if (pTemp)
		return pTemp->DataStr2;
	else
		return TEXT("");
}

LPVOID CBList::GetDatapVoid( int iIndex )
{
	NODE* pTemp = NodeFromIndex(iIndex);
	if (pTemp)
		return pTemp->pDataElse;
	else
		return NULL;
}

NODE* CBList::NodeFromIndex( int iIndex )
{
	//如果超出链表长度，直接返回失败
	if (iIndex >= m_ilistLen || iIndex < 0)
		return NULL;

	NODE *pTemp = m_ListHead;
	int j = 0;
	//仅仅当链表指针为真，并且当前的检索位置小于i的时候向后进行检索
	while (pTemp && j<iIndex)
	{
		pTemp = pTemp->next;
		j++;
	}

	//如果是空的，那么就是检索失败，即位置超出了最长长度
	if (pTemp == NULL) 
		return NULL;
	else
		return pTemp;	//返回对应节点的那个地址
}
