#include "BForm.h"


//节点的结构体，此处的 data 为 int 数据
struct NODE
{
	int DataInt;			//链表对应的整形数据
	int DataInt2;			//链表附加的整形数据2
	long DataLong;		//链表的附加 double 数据
	long DataLong2;		//链表的附加 double 数据2
	tstring DataStr;		//附加的字符串数据
	tstring DataStr2;		//附加的字符串数据2
	LPVOID pDataElse;		//针对用户可能需要的其他类型的数据，此处提供一个指针接口，用户可以用链表来存任何想要存的内容
	NODE* next;
};

//////////////////////////////////////////////////////////////////////////
//单链表的类实现
//Made By: ZhiZhi
//时间：2020:11:10
//////////////////////////////////////////////////////////////////////////
class CBList
{
public:

	//构造函数
	CBList();

	//析构函数
	~CBList();

	//清空链表的内容
	void ListClear();

	//判断链表是否为空
	bool IsEmpty();

	//获得链表的长度
	int GetListLength();

	//获得对应空间的数值内容
	//i 表示要获得的是哪个数据，从 0 开始
	//pData 表示要获得的数据的地址
	bool GetElemFromIndex(int i, int *pData);

	//定位链表数据的位置，返回对应的链表中的位置
	//返回值为对应的链表的位置，从 0 开始，没有找到返回 -1
	int LocateElem(int iData);

	//获得对应内容的前一个内容是什么
	//cur_e 表示要查找的内容
	//pre_e 表示的是要放置上一个数据的地址
	bool GetPriorElem(int cur_e, int *pre_e);

	//获得对应内容后一个内容是什么
	//cur_e 表示要查找的内容
	//next_e 表示的是要放置下一个数据的地址
	bool GetNextElem(int cur_e, int *next_e);

	//在对应位置插入内容，插入成功，返回插入位置，失败，返回 -1
	//iDataInsert 表示要插入的内容，默认是插入 DataInt 数值
	//iIndex 表示的是插入的位置，默认是 0 ，即头节点
	int ListInsert(int iDataInsert, int iIndex=0);

	//删除对应空间的内容
	bool ListDelete(int iIndex);

	//翻转对应的链表的内容，并且返回头节点
	NODE* ListReverse();

	//设置对应附加数据的内容
	bool SetDataInt(int iIndex, int iData);
	bool SetDataInt2(int iIndex, int iData);
	bool SetDataLong(int iIndex, long dData);
	bool SetDataLong2(int iIndex, long dData);
	bool SetDataStr(int iIndex, LPCTSTR sData);
	bool SetDataStr2(int iIndex, LPCTSTR sData);
	
	//通过排列的位置获得对应附加数据的内容
	int GetDataInt(int iIndex);
	int GetDataInt2(int iIndex);
	long GetDataLong(int iIndex);
	long GetDataLong2(int iIndex);
	tstring GetDataStr(int iIndex);
	tstring GetDataStr2(int iIndex);

	//====================================== 核心技术 ===============================================
	//适用指针使得此链表类可以保存任何类型的数据，注意使用的时候为了统一，全部需要采用new的方式动态开辟空间
	//本类会在清理节点的时候自动清理对应的内存空间，有了这个功能，本链表的类就可以存储任何类型的数据了
	//==========================================================
	//使用示例：（在节点保存一个POINT类型的数据，此类型的数据不在结构体的定义中）
	// 	CBList lstTest;
	// 	POINT* pt = new POINT;		//注意注意：此处的变量一定是 new 的空间，如果不是会在析构的时候出现错误，
	//								//因为不是 new 的空间不能够被 delete 释放
	// 	pt->x = 10;
	// 	pt->y = 5;
	// 	lstTest.ListInsert(1,0);			//在第0个位置插入链表节点，节点的整形数据为1
	// 	lstTest.NodeDatapVoidSet(0,pt);		//将pt的地址保存到这个节点里面
	//	//一定要注意此处的类型转换：
	//	//①首先将返回的 LPVOID 类型的指针转换为 POINT类型的指针，
	//	//②然后将 LPVOID里面的 LONG 类型数据转换为 int 类型数据调用Str函数用消息框显示出来
	// 	MsgBox(Str(int(((POINT*)(lstTest.NodeDatapVoid(1))/*①*/)->x/*②*/) + int(((POINT*)(lstTest.NodeDatapVoid(1)))->y)));
	//	输出结果是 15 (15 = 10 + 5)
	//===========================================================
	LPVOID GetDatapVoid(int iIndex);
	bool SetDatapVoid(int iIndex, LPVOID pData);

	//获得链表的头部指针
	NODE* GetListHead();

private:
	//通过下标获得对应的节点的指针
	//为了增加检索速度，应该增加一个函数，可以从指定的节点进行查找，加快速度
	NODE* NodeFromIndex(int iIndex);

private:
	NODE* m_ListHead;	//保存类里面的链表的头部
	int m_ilistLen;		//对应链表的长度，省得还得循环一遍
};







