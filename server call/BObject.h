#pragma once
#include "BForm.h"
#include "BWindGDI.h"
#include <math.h>
#include <vector>
#include <time.h>
#include <map>
using namespace std;

//主要是飞机的种类，可以在此处添加对应的飞机内容
enum EPlaneType
{
	ePlaneNone = 0,
	ePlaneAr234 = 1,
	ePlaneB17 = 2,
	ePlaneB24 = 3,
	ePlaneB25 = 4,
	ePlaneBa349 = 5,
	ePlaneBf109 = 6,
	ePlaneBf110 = 7,
	ePlaneBv138 = 8,
	ePlaneC43 = 9,
	ePlaneCg4a = 10,
	ePlaneDo17 = 11,
	ePlaneDo24 = 12,
	ePlaneDo335 = 13,
	ePlaneF3F = 14,
	ePlaneF4F = 15,
	ePlaneF4U = 16,
	ePlaneF80 = 17,
	ePlanefokd = 18,
	ePlaneFw189 = 19,
	ePlaneFw190 = 20,
	ePlaneHe111 = 21,
	ePlaneHe115 = 22,
	ePlaneHe162 = 23,
	ePlaneHo229 = 24,
	ePlaneHs123 = 25,
	ePlaneJu52 = 26,
	ePlaneJu87 = 27,
	ePlaneJu88 = 29,
	ePlaneJu188 = 30,
	ePlanelippisch = 31,
	ePlaneMe163 = 32,
	ePlaneMe262 = 33,
	ePlaneP38 = 34,
	ePlaneP39 = 35,
	ePlaneP40 = 36,
	ePlaneP47 = 37,
	ePlaneP51 = 38,
	ePlaneP61 = 39,
	ePlaneSBD = 40
};

//枚举，进行飞机机动动作的判定
enum EPlanAction
{
	eActionNone = 0,				//无动作
	eActionTurnSharpLeft = 1,		//急左转
	eActionTurnSharpRight = 2,		//急右转
	eActionPullUp = 3,				//上拉升
	eActionPullUpRecover = 4,		//上拉升恢复
	eActionRollLeft = 5,			//左侧翻滚
	eActionRollRight = 6,			//右侧翻滚
	eActionTurnLeft = 7,			//左转
	eActionTurnRight = 8,			//右转
	eActionAttack = 9				//攻击
};

//主武器弹药的种类
enum EMainWeaponType
{
	eWeaponNone = 0,
	eWeaponM1_762_2 = 1,	//M1机枪:7.62mm*2
	eWeaponM1_762_4 = 2,	//M1机枪:7.62mm*4
	eWeaponM2_127_2 = 3,	//M2机枪:12.7mm*2
	eWeaponM2_127_4 = 4,	//M2机枪:12.7mm*4
	eWeaponM3_127_2 = 5,	//M3机枪:12.7mm*2
	eWeaponM3_127_4 = 6,	//M3机枪:12.7mm*4
};

//物品各项属性的 object
struct SObject
{
	short int x;				//在源图的 x 位置
	short int y;				//在源图的 y 位置
	short int width;			//在源图的宽度
	short int height;			//在源图的高度
	float frameX;			//实际在主框图显示的坐标 x 位置
	float frameY;			//实际在主框图显示的坐标 y 位置
	short int frameWidth;		//实际显示的 width
	short int frameHeight;		//实际显示的 height
};

//动态物品的各项属性内容
struct SDynamicObj 
{
	float bSpeed;				//正常运动的速度
	float bSpeedX;				//主要是为了便于计算，此处直接保存每次 x 方向的移动的距离
	float bSpeedY;				//主要是为了便于计算，此处直接保存每次 y 方向的移动的距离
};

//子弹的各项属性的结构体
struct SBullet 
{
	BYTE bHurt;			//子弹的伤害数值
	//其他的以后想起来再说吧，哈哈哈哈
};

//飞机各项属性的结构体
struct SPlane
{
	short int bHealth;						//飞机的生命值
	float fSpeed;							//正常运动的速度
	DWORD dwReloadTime;						//重装弹药的时间
	DWORD dwAmmoNum;						//弹药量，不是无限的弹药量
	BYTE bTurnSpeed;						//飞机正常转弯的速度，角度制
	BYTE bSharpTurnSpeed;					//飞机急转弯的速度，角度制
	BYTE bMainAttackPower;					//飞机主武器弹药的单发子弹伤害
	BYTE bAssistantWeapon;					//飞机副武器弹药的单发伤害（不一定是子弹）
	EMainWeaponType eMainWeaponType;		//飞机主武器弹药的攻击方式
	float bBulletSpeed;						//子弹的飞行速度，不同的飞机飞行速度还是不同吧
};

//结构体主要用于存储不同的飞机型号和对应的翻滚以及俯仰的帧数据
struct SPlaneParameters
{
	tstring sPlanePicPath;				//表示的是图片的文件路径
	vector<SObject> vRollFrames;		//表示的是翻滚的帧数据
	vector<SObject> vPitchFrames;		//表示的是俯仰的帧数据
	SPlane sPlane;						//表示飞机的各种属性内容
};

//结构体主要用于存储不同的飞机型号和对应的翻滚以及俯仰的帧数据
struct SExplodeFrames
{
	vector<SObject> vExplodeFrames;		//表示的是爆炸的帧数据
};

 
