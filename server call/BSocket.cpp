//////////////////////////////////////////////////////////////////////////
// mdlSocket.h 用于套接字的通用模块内容
// 实现 windows 套接字的相关操作
//
// Programmed by Zhizhi
// Last Modified: 2020-7-30
//////////////////////////////////////////////////////////////////////////

#include "BSocket.h"


CBSocket::CBSocket()
{
	//初始化所有的类内部的变量内容
	m_sSocketLocal = INVALID_SOCKET;
	//客户端的socket采用动态开辟空间的方式
	//刚开始没有客户端连接，所以此处首先将它作为一个空指针
	m_pSocketClient = NULL;
	m_pDataReceive = 0;
	m_hWnd = NULL;
	m_fConnected = false;
	m_fIsServer = false;
	m_sSocketAddr.sin_family = AF_INET;
	m_sSocketAddr.sin_port = 0;
	m_sSocketAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	m_iClientCount = 0;
	m_sckState = esckClosed;

	//为发送数据的缓冲区扩展10240字节的空间
	m_sSocketData.sData = new char[10240];
	m_sSocketData.msgHead.iNewLength = 10240;
	//为接收数据的缓冲区扩展10240字节的空间
	m_pDataReceive = new char[10240];

	// ====================初始化TCP协议=====================
	WSADATA wsaData;
	BOOL ret = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (ret != 0)
	{
		MsgBox(TEXT("初始化套接字失败!"));
		return;
	}

}

CBSocket::CBSocket(HWND hWnd)
{
	//首先初始化 socket 的环境
	CBSocket();
	//然后通过这个句柄创建 socket 的句柄，然后注册异步事件，统一做成非阻塞性
	OnCreate(hWnd);

}

SOCKET CBSocket::OnCreate(HWND hWnd)
{
	//如果已经创建过，那么就直接返回原来的那个
	if (m_sSocketLocal != INVALID_SOCKET)
		return m_sSocketLocal;

	//此处可以改变设置对应的socket的通讯类型
	m_sSocketLocal = socket(AF_INET/*Internet 协议*/,
		SOCK_STREAM/*套接字类型*/,
		0/*与特定的地址族相关的协议，为0自动选择*/);

	if (m_sSocketLocal == INVALID_SOCKET)	//创建失败
		return 0;

	//注册网络异步事件,m hWnd为应用程序的主对话框或主窗口的句柄
	//默认所有的网络事件都注册，m_sSocketLocal 是消息接收到的 socket 的句柄
	if (WSAAsyncSelect(m_sSocketLocal, hWnd, NETWORK_EVENT, FD_ACCEPT | FD_CLOSE | FD_READ | FD_WRITE) != 0)
	{
		OnClose(m_sSocketLocal);
		return 0;
	}

	//将创建成功的套接字的窗口的句柄保存到类里面的变量
	m_hWnd = hWnd;
	m_sckState = esckOpen;	//设置状态为打开状态
	return m_sSocketLocal;	//如果创建成功，返回套接字的句柄
}

HWND CBSocket::IdentifiedWindow()
{
	return m_hWnd;
}

bool CBSocket::IdentifiedWindowSet(HWND hWnd)
{
	//注册网络异步事件,m hWnd为应用程序的主对话框或主窗口的句柄
	//覆盖掉原来的默认窗口的设定
	if (WSAAsyncSelect(m_sSocketLocal, hWnd, NETWORK_EVENT, FD_ACCEPT | FD_CLOSE | FD_READ | FD_WRITE) != 0)
	{
		OnClose();
		return false;
	}
	//否则更新窗口句柄，并且返回成功
	m_hWnd = hWnd;
	return true;
}

bool CBSocket::OnConnect(LPCTSTR sIPAddress, int iPort)
{
	if (m_sSocketLocal == INVALID_SOCKET && m_hWnd)
	{
		OnCreate(m_hWnd);
	}

	//如果没有创建套接字，但是绑定的窗口句柄已知，直接创建一个
	if ((m_sckState == esckConnected || m_sckState == esckConnectionPending) && m_hWnd)
	{
		OnClose(m_sSocketLocal);
		OnCreate(m_hWnd);
	}

	//由于 char 和 TCHAR 不同，所以此处需要做出进一步的操作来相互转换
	char c_ip[128];
	TcharToChar(sIPAddress, c_ip);
	//如果当前是连接状态，那么就先断开当前的连接，然后重新连接新的服务器
	if (m_fConnected)
	{
		if (m_sSocketAddr.sin_port != htons(iPort) || m_sSocketAddr.sin_addr.s_addr != inet_addr(c_ip))
		{
			//关闭原来的socket，重新创建一个新的socket用于连接
			OnClose();
			OnCreate(m_hWnd);
			//设置新的端口号和 ip 地址
			m_sSocketAddr.sin_port = htons(iPort);		//端口号
			//此处设置连接的服务器的 ip 地址
			m_sSocketAddr.sin_addr.s_addr = inet_addr(c_ip);
		}
	}
	else
	{
		//设置新的端口号和 ip 地址
		m_sSocketAddr.sin_port = htons(iPort);		//端口号
		//此处设置连接的服务器的 ip 地址
		m_sSocketAddr.sin_addr.s_addr = inet_addr(c_ip);
	}

	//连接主机
	// 吐了，他这里直接返回就是发送成功，但是没有很快的建立连接，所以此处感觉需要用循环发送和超时的设定
	// 所以此处是核心代码感觉，真的挺复杂的感觉
	//此处如果正常的连接成功，会收到一个系统的消息，所以此处仅仅要做的就是发送消息，具体就要来通过系统的消息来显示是否连接成功
	int iError = 0;
	connect(m_sSocketLocal, (struct sockaddr*)&m_sSocketAddr, sizeof(struct sockaddr));

	if (WSAEWOULDBLOCK != (iError = WSAGetLastError()))
	{
		//如果失败了，自动关闭这个，然后重新创建一个新的，返回新的连接情况
		OnClose(m_sSocketLocal);
		return false;
	}
	else
		m_sckState = esckConnectionPending;	//连接挂起状态

	return true;
}

CBSocket::~CBSocket()
{
	//主要用于最终资源的释放和端口的断开
	OnClose(0);

	//动态空间的释放
	if (m_pDataReceive)	//接收数据的空间
	{
		delete[]m_pDataReceive;
		m_pDataReceive = NULL;
	}
	if (m_sSocketData.sData)	//发送数据的空间
	{
		delete[]m_sSocketData.sData;
		m_sSocketData.sData = NULL;
	}

	WSACleanup();
}

SOCKET CBSocket::hWndSocket()
{
	return m_sSocketLocal;
}

int CBSocket::ClientCount()
{
	return m_iClientCount;
}

SOCKET CBSocket::GetSocket(int iSocket)
{
	//如果超出范围直接返回-1，否则返回指定的socket句柄
	if (iSocket > m_iClientCount || m_iClientCount == 0)
		return -1;
	else
		return m_pSocketClient[iSocket];

}

void CBSocket::SocketEventsGenerator(int iSocket, int iEvent, int iError)
{
	//===============================================================================
	//此处类里面对消息进行预处理，主要针对的是例如发送消息的缓冲区满或者其他的函数
	//===============================================================================
	switch (iEvent)
	{
	case FD_WRITE:
		//如果键值存在，改变类记录的状态内容
		if (m_sckState == esckConnectionPending && !m_fIsServer)
		{
			m_sckState = esckConnected;
			m_fConnected = true;
		}
		else if (m_sckState == esckSendPending)
		{
			//发送未发送完成的内容
			OnSend(NULL, 0, 0, iSocket);
		}
		break;
	case FD_READ:
	{
		//进行读取操作，如果读取未完成，此处自动继续读取

	}
	break;
	default:
		break;
	}
}

MSGHEAD CBSocket::DataHeadRecv()
{
	return m_msgHead;
}

char* CBSocket::DataRecv()
{
	return m_pDataReceive;
}

int CBSocket::OnSend(char* cDataSend, int iLength, DWORD cmdID, SOCKET sSocketSend /*= NULL*/)
{
	//如果此时没有创建一个socket句柄，那么直接返回
	if (m_sSocketLocal == INVALID_SOCKET)
		return -1;
	//如果是客户端并且尚未建立连接，直接返回
	if (!m_fIsServer && !m_fConnected)
		return -1;
	//如果是服务器，并且没有客户端连接，也返回失败
	if (m_fIsServer && ClientCount() == 0)
		return -1;

	static int iSendLength = 0;			//发送数据的总长度
	static int iSendSuccesslength = 0; //保存发送的字节数，便于下次如果没有发送成功而继续发送

	if (m_sckState != esckSendPending)
	{
		//设置发送内容
		iSendLength = sizeof(MSGHEAD) + iLength;		//真正要发送的数据的长度
		//设置发送的数据包的内容
		m_sSocketData.msgHead.iDataLength = iLength;		//数据除去数据包的有效的长度
		m_sSocketData.msgHead.dwcmdID = cmdID;

		//首先判断当前的空间大小是否够
		//如果不够需要动态扩增
		if (m_sSocketData.msgHead.iNewLength < iLength && iLength < MAX_LENGTH)
		{
			if (m_sSocketData.sData)
			{
				delete[]m_sSocketData.sData;
				m_sSocketData.sData = NULL;
			}

			//然后申请指定的空间长度，改变记录项
			m_sSocketData.sData = new char[iLength];
			m_sSocketData.msgHead.iNewLength = iLength;
		}

		CopyMemory(m_sSocketData.sData, cDataSend, iLength);	//这个地方发送的数据量不能过大，否则就会出现内存越位，改变本不该改变的变量内容

	}

	//===================================== 发送数据 ==========================================
	//如果是服务器，可以指定发送的数据的 socket 的句柄，如果最后一个参数是空白，默认是给全部的客户端发送消息
	//通过不同的情景判断不同的待发送的socket的句柄
	SOCKET sSocket = 0;
	if (!sSocketSend)
	{
		if (m_fIsServer)
			sSocket = m_pSocketClient[0];
		else
			sSocket = m_sSocketLocal;
	}
	else
		sSocket = sSocketSend;

	//调用发送函数
	//////////////////////////////////////////////////////////////////////////
	//千万注意，动态内存空间的存储不是连续存储的，需要另外发送
	//此处吃的亏真的烦，首先发送包头，然后发送主体部分
	//////////////////////////////////////////////////////////////////////////
	send(sSocket, (char*)&m_sSocketData.msgHead, sizeof(MSGHEAD), 0);
	iSendSuccesslength += send(sSocket, m_sSocketData.sData, iLength - iSendSuccesslength, 0);

	if (iSendSuccesslength < iLength)	//如果发送的长度不够
	{
		m_sckState = esckSendPending;
		return iSendSuccesslength;		//返回当前已经发送的数据长度
	}
	else
	{
		iSendSuccesslength = 0;
		m_sckState = esckConnected;
		return iSendLength;			//返回这个数据包的长度吧
	}
}

int CBSocket::Onsend(char* cDataSend, int iLength, SOCKET sSocketSend /*= NULL*/)
{
	//如果此时没有创建一个socket句柄，那么直接返回
	if (m_sSocketLocal == INVALID_SOCKET)
		return -1;
	//如果是客户端并且尚未建立连接，直接返回
	if (!m_fIsServer && !m_fConnected)
		return -1;
	//如果是服务器，并且没有客户端连接，也返回失败
	if (m_fIsServer && ClientCount() == 0)
		return -1;

	//如果是服务器，可以指定发送的数据的 socket 的句柄，如果最后一个参数是空白，默认是给全部的客户端发送消息
	//通过不同的情景判断不同的待发送的socket的句柄
	SOCKET sSocket = 0;
	if (!sSocketSend)
	{
		if (m_fIsServer)
			sSocket = m_pSocketClient[0];
		else
			sSocket = m_sSocketLocal;
	}
	else
		sSocket = sSocketSend;

	//直接发送内容
	return send(sSocket, cDataSend, iLength, 0);
}

bool CBSocket::OnReceive(char** pSocketData, MSGHEAD* msgRecv, SOCKET sSocketRecv)
{
	//如果此时没有创建一个socket句柄，那么直接返回
	if (m_sSocketLocal == INVALID_SOCKET)
		return false;

	//采用静态的数据是为了适应多次接收的情况
	static int iDataLength = 0;					//用于统计当前接收到的信息的长度
	static bool fRecvHeadOK = false;			//表示表头是否接收完整
	int iLen = 0;							//用于暂时记录每一次接收到的消息信息
	int iMsgHeadLength = sizeof(MSGHEAD);	//这是数据包头的长度

	(*msgRecv).iDataLength = 0;	//这个数据仅仅会在接收到完整的数据包的包体才会被更新，否则为 0

	//首先进行接收表头，测试版本
	iLen = recv(m_fIsServer ? sSocketRecv : m_sSocketLocal, (char*)(&m_msgHead) + iDataLength, iMsgHeadLength - iDataLength, 0);

	if (iLen == iMsgHeadLength - iDataLength)	//如果表头刚好完整接收，更新包体长度，清空变量
	{
		//数据长度不合法，直接丢弃，返回失败
		if (m_msgHead.iDataLength >= 0 && m_msgHead.iDataLength <= MAX_LENGTH)
		{
			if (m_msgHead.iDataLength == 0)
			{
				//如果数据长度为 0 ，并且数据头接收完毕，直接清空变量内容，然后返回 true
				fRecvHeadOK = false;
				(*msgRecv).iDataLength = m_msgHead.iDataLength;
				(*msgRecv).dwcmdID = m_msgHead.dwcmdID;
				m_msgHead.iDataLength = 0;
				iDataLength = 0;
				*pSocketData = m_pDataReceive;
				return true;
			}
			//如果接收成功，更新 fRecvHeadOK 和已经接收到的内容长度 iDataLength
			iDataLength = 0;
			fRecvHeadOK = true;
		}
		else
		{
			iDataLength = 0;
			fRecvHeadOK = false;
			return false;
		}
	}
	else if (iLen < iMsgHeadLength - iDataLength && iLen >= 0)
	{
		//如果内容不够，返回成功，但是包头接收标志仍为 false
		iDataLength += iLen;
		fRecvHeadOK = false;
	}
	else	//数据接收错误
	{
		iDataLength = 0;
		fRecvHeadOK = false;
		return false;
	}

	//=====================直接接收数据===================
	if (fRecvHeadOK)
	{
		//如果接收过表头，并且是首次进入接收模式
		//采用动态扩展的方式来扩展空间，如果是已经扩展过，那么直接跳过

		iLen = 0;	//上面变量的清理

		if (!iDataLength)
		{
			//此处为了增加运行效率，采用缓冲区超出动态定义的方式，可以有效的减少开辟空间造成的开销
			static int iNewSize = 10240;	//默认的初始地址大小

			//如果空间不足，那么开辟新的空间，更新空间大小的变量，否则就直接
			if (iNewSize < m_msgHead.iDataLength)
			{
				delete[]m_pDataReceive;
				m_pDataReceive = NULL;
				iNewSize = m_msgHead.iDataLength;
				m_pDataReceive = new char[m_msgHead.iDataLength];	//开辟新的内存空间
			}
			else
				memset(m_pDataReceive, 0, iNewSize);

		}

		//开始接收数据内容
		iLen = recv(m_fIsServer ? sSocketRecv : m_sSocketLocal, m_pDataReceive + iDataLength, m_msgHead.iDataLength - iDataLength, 0);

		if (iLen == m_msgHead.iDataLength - iDataLength)
		{
			//如果表头刚好完整接收，改变标志变量，数据长度，数据的指针
			fRecvHeadOK = false;
			(*msgRecv).iDataLength = m_msgHead.iDataLength;
			(*msgRecv).dwcmdID = m_msgHead.dwcmdID;
			m_msgHead.iDataLength = 0;
			m_msgHead.dwcmdID = 0;
			iDataLength = 0;
			*pSocketData = m_pDataReceive;
		}
		else if (iLen <= m_msgHead.iDataLength - iDataLength && iLen >= 0)
		{
			//如果接收内容不够，改变接收的指针位置
			iDataLength += iLen;
			fRecvHeadOK = true;
		}
		else	//数据接收错误
		{
			//如果接收失败，直接清空内容
// 			iDataLength = 0;
// 			m_msgHead.iDataLength = 0;
//			fRecvHeadOK = false;
			return false;
		}
	}

	return true;
}

int CBSocket::OnReceive(char* cDataRecv, int iLength, SOCKET sSocketRecv)
{
	//如果此时没有创建一个socket句柄，那么直接返回
	if (m_sSocketLocal == INVALID_SOCKET)
		return false;

	//接受内容，返回接收到的数据长度
	return recv(sSocketRecv, cDataRecv, iLength, 0);
}

bool CBSocket::OnBind(int iPort)
{
	//如果此时没有创建socket，那么首先检测此时的窗口句柄是否为真
	//如果为真，那么直接创建一个服务器的socket
	if (m_sSocketLocal == INVALID_SOCKET && m_hWnd)
		OnCreate(m_hWnd);

	//如果此时创建的socket已经绑定到了一个端口，那么关闭原来的，重新创建一个用来绑定
	if (m_iPort)
	{
		OnClose(m_sSocketLocal);
		OnCreate(m_hWnd);
	}

	//绑定到本地一个端口上
	sockaddr_in localaddr;
	localaddr.sin_family = AF_INET;
	localaddr.sin_port = htons(iPort);	//端口号

	localaddr.sin_addr.S_un.S_addr = INADDR_ANY;		//这个表示是检测本机的所有ip
	//如果计算机有多块网卡，不希望在全部的网卡上进行监听，那么就将该字段填写成内网的ip就行
	//套接字会在指定的ip地址监听

	if (bind(m_sSocketLocal, (const struct sockaddr*)&localaddr, sizeof(sockaddr)) == SOCKET_ERROR)
	{
		OnClose(m_sSocketLocal);
		OnCreate(m_hWnd);
		return false;
	}
	//保存端口号
	m_iPort = iPort;
	m_fIsServer = true;
	return true;
}

void CBSocket::OnClose(SOCKET sSocketClose /*=0*/)
{
	//关闭当前的连接的端口，如果有连接的话

	//如果是服务器，那么直接就关闭，如果不是服务器，判断是否开启连接
	if (m_fIsServer)
	{
		int i;
		//如果是服务器，并且全部关闭，关闭本地的服务器 socket
		if (sSocketClose == 0 || sSocketClose == m_sSocketLocal)
		{
			for (i = 0; i < m_iClientCount; i++)
			{
				//逐个关闭每一个客户端的连接
				closesocket(m_pSocketClient[i]);
			}
			//然后清空动态开辟的空间
			if (m_pSocketClient)
			{
				delete[]m_pSocketClient;
				m_pSocketClient = NULL;
				m_iClientCount = 0;			//客户端数量直接清零
			}

			//关闭本地的服务器的端口
			closesocket(m_sSocketLocal);
			m_sSocketLocal = INVALID_SOCKET;
			m_iPort = 0;
			m_sckState = esckClosed;
		}
		else
		{
			//如果当前没有客户端连接，那么就直接返回就好了
			if (m_iClientCount <= 0)
				return;

			//如果当前的客户端数量至少为一个，那么就进行下面的关闭操作
			SOCKET* pSocketTemp = NULL;
			bool fClose = false;
			pSocketTemp = new SOCKET[m_iClientCount - 1];
			//否则，关闭指定的客户端的socket的连接，然后更新这个动态socket数组的内容
			for (i = 0; i < m_iClientCount; i++)
			{
				//如果相等的话就关闭，然后进入另一循环拷贝后面的socket句柄
				if (m_pSocketClient[i] == sSocketClose)
				{
					fClose = true;
					closesocket(sSocketClose);
					break;
				}
				//由于开辟的空间有限，如果i == m_iClientCount-1，那么就证明要关闭的socket不存在，所以直接跳出循环，否则内存越界
				if (i == m_iClientCount - 1)	break;

				pSocketTemp[i] = m_pSocketClient[i];	//如果没有出现关闭的客户端，直接复制内容
			}

			//此处有两种情况，如果此处的关闭无效，那么原来的空间就不能够改变
			if (!fClose)
			{
				delete[]pSocketTemp;
				pSocketTemp = NULL;
			}
			else
			{
				m_iClientCount--;		//如果程序运行到这个位置，表明，前面已经成功关闭一个socket

				for (; i < m_iClientCount; i++)
				{
					//拷贝关闭的socket后面的一个的socket句柄
					pSocketTemp[i] = m_pSocketClient[i + 1];
				}
				//拷贝完毕后，直接删除原来的空间，指针指向新的内存空间
				if (m_pSocketClient)
					delete[]m_pSocketClient;

				m_pSocketClient = pSocketTemp;		//指向新的空间
			}

		}
	}
	else
	{
		if (m_fConnected)
		{
			closesocket(m_sSocketLocal);
			m_fConnected = false;
			m_sSocketLocal = INVALID_SOCKET;
		}
		m_sckState = esckClosed;
	}
}

bool CBSocket::OnListen(int iMaxClient/*=1*/)
{
	//如果没有创建套接字，那么就直接返回失败
	if (!m_sSocketLocal || !m_fIsServer)
		return false;

	listen(m_sSocketLocal, iMaxClient);//设置侦听模式
	m_sckState = esckListening;
	return true;
}

bool CBSocket::OnAccept()
{
	//如果没有创建套接字，或者不是服务器，那么就直接返回失败
	if (m_sSocketLocal == INVALID_SOCKET || !m_fIsServer)	return false;

	//=============================================================
	//此处开始连接前，首先增加空间用来记录这个新的客户端 socket 句柄
	m_iClientCount++;
	SOCKET* pSocketTemp;
	int i = 0;
	pSocketTemp = new SOCKET[m_iClientCount];

	//首先将原有内容拷贝到新的空间中
	for (i = 0; i < m_iClientCount - 1; i++)
	{
		pSocketTemp[i] = m_pSocketClient[i];
	}

	//然后删除原空间，将原有指针指向新的地址
	if (m_pSocketClient)
		delete[]m_pSocketClient;
	m_pSocketClient = pSocketTemp;		//指向新的地址
	//===============================================================

	//接收连接请求
	int len = sizeof(sockaddr);
	//调用API函数,接受连接,并返回一个新套接字
	//还可以获得客户端的IP地址
	m_pSocketClient[m_iClientCount - 1] = accept(m_sSocketLocal,
		(struct sockaddr*)&m_sSocketAddr, &len);

	if (m_pSocketClient[m_iClientCount - 1] < 0)
	{
		MsgBox(StrAppend(TEXT("连接失败，请重试！！\r\n"), Str(WSAGetLastError())));
		return false;
	}
	else
	{
		m_fConnected = true;
		return true;
	}
}










