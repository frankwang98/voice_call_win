//////////////////////////////////////////////////////////////////////////
// mdlSocket.h 用于套接字的通用模块内容
// 实现 windows 套接字的相关操作
//
// Programmed by Zhizhi
// Last Modified: 2020-7-30
//////////////////////////////////////////////////////////////////////////

#include "BForm.h"
#include "BWindows.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <WinSock.h>
#include <errno.h>

#pragma comment(lib, "ws2_32.lib")

struct MSGHEAD
{
	int iDataLength;				//包头，里面记录了真实数据的大小
	DWORD dwcmdID;			//消息的类型，用于区分不同类型的消息
	int iNewLength;		//这个结构体的char*指针对应空间的占有的字节数
};
//定义结构体用于存储数据包的内容
struct SOCKETDATA
{
	MSGHEAD msgHead;	//消息的头部数据
	char* sData;		//真实数据：里面就是所要发送的真实数据
};
enum SocketState
{
	esckClosed = 0,					//没有创建socket
	esckOpen = 1,					//已经成功创建socket
	esckListening = 2,				//正在监听
	esckConnectionPending = 3,		//连接挂起，针对客户端的状态
	esckConnected = 4,				//连接成功，针对客户端的状态
	esckSendPending = 5				//发送挂起状态，需要自动的
};

//定义每一个数据包的最大大小
#define MAX_LENGTH 8388608-sizeof(MSGHEAD)

class CBSocket
{
public:
	//////////////////////////////////////////////////////////////////////////
	// 公有从成员函数
	//////////////////////////////////////////////////////////////////////////

	//构造函数
	CBSocket();
	//如果在创建的同时给出了这个 socket 连接的窗口句柄，自动执行初始化的操作
	CBSocket(HWND hWnd);

	//析构函数
	~CBSocket();

	//初始化，创建一个 socket 的句柄，属于句柄为 hWnd 的窗口
	//如果创建失败，返回 0
	SOCKET OnCreate(HWND hWnd);

	//获得和设置socket的接收消息的窗口的句柄，便于多窗体共用一个socket进行操作
	HWND IdentifiedWindow();
	bool IdentifiedWindowSet(HWND hWnd);

	//如果一个套接字被成功创建，那么开始连接，连接函数
	//特别注意，本函数仅仅发送连接，如果连接成功，那么会在客户端收到一个 FD_WRITE 的消息，代表连接成功
	// sIPAddress ip 地址的字符串形式
	// iPort 要连接的端口号
	bool OnConnect(LPCTSTR sIPAddress, int iPort);

	//******************************************************************************
	//发送数据的函数，直接输入要发送的内容，本函数返回值为发送的字节长度，如果失败，返回 -1
	//如果一次不能全部发送，本类的兄啊西处理函数会在适当的时机自动发送剩余的数据
	//******************************************************************************
	//cDataSend为待发送数据的地址，需要使用强制地址转换为 char* 类型的
	//iLength 为待发送的数据长度
	//cmdID 这个是消息头部的命令ID，主要用于唯一的标记一种消息
	//sSocketSend 表示的是要发送的socket的句柄
	//******************************************************************************
	//用法介绍：
	//TCHAR msg[] = TEXT("......");
	//g_sSocketAccount.OnSend((char*)&msg, sizeof(msg),eMsgType1);
	int OnSend(char* cDataSend, int iLength, DWORD cmdID, SOCKET sSocketSend = NULL);

	//默认的 socket 的函数的封装，不对其做任何操作
	// cDataSend 发送的数据的首地址
	// iLength 发送的数据长度
	// sSocketSend 待发送的 socket 的句柄内容
	int Onsend(char* cDataSend, int iLength, SOCKET sSocketSend = NULL);

	//接收数据的函数，用于数据的接收，封装 API
	//直接返回数据的字符串形式，必须指定要接收数据的socket句柄内容
	//*****************************************************************************
	//char **p;
	//MSGHEAD msgHead;
	//g_sSocketAccount.OnReceive((char**)&msgResp, &msgHead, iSocket);
	//switch(msgHead.dwCmdID)
	// {
	// 	case:
	// 		...
	//	default:
	//		break;
	// }
	bool OnReceive(char** pSocketData, MSGHEAD* iLength, SOCKET sSocketRecv);

	//默认的 socket 的函数的封装，不对其做任何操作
	// cDataRecv 可用的接收数据缓冲区首地址
	// iLength 可用的接收数据的长度
	// sSocketRecv 待接收的 socket 的句柄
	int OnReceive(char* cDataRecv, int iLength, SOCKET sSocketRecv);

	//关闭连接
	//用户需要指定要关闭的socket的句柄来关闭，如果要关闭服务器，本函数会自动关闭所有的客户端的连接，然后关闭本地的socket
	//默认是-1，即关闭本地的socket
	void OnClose(SOCKET sSocketClose = 0);

	//绑定一个本地的地址
	bool OnBind(int iPort);

	//开始侦听
	//iMaxClient 为最多的可以连接的客户端
	bool OnListen(int iMaxClient = 1);

	//如果接收到了连接请求，然后就连接
	bool OnAccept();

	//获得内部的 socket 的句柄
	SOCKET hWndSocket();

	//如果是服务器的话，获得此时服务器连接的客户端的数量
	int ClientCount();

	//获得此时的某一个socket的句柄内容，如果超出此时的最大个数，返回-1
	SOCKET GetSocket(int iSocket);

	//增加消息处理函数，能够动态的获得本地socket的状态
	void SocketEventsGenerator(int iSocket, int iEvent, int iError);

	//如果接收成功，本函数返回对应的数据包的头部信息
	MSGHEAD DataHeadRecv();

	//返回对应的数据的内容，如果头部有效，此处内容才有效，否则是个空指针
	char* DataRecv();

private:
	//////////////////////////////////////////////////////////////////////////
	// 类内部的私有变量
	//////////////////////////////////////////////////////////////////////////

	SOCKET m_sSocketLocal;					//类内部创建的 socket 的句柄
	int m_iClientCount;						//如果是服务器，统计当前所有的连接客户端个数
	SOCKET* m_pSocketClient;				//如果接收连接的时候可以存储连接的客户端的 socket 的句柄
											//此处决定以动态开辟客户端的socket句柄					

	HWND m_hWnd;						//socket 关联的窗口的句柄
	sockaddr_in m_sSocketAddr;			//这个是用来保存类内部的 socket 的句柄连接的 socket 的具体内容
										//如果是客户端，保存的便是连接的目标的 ip 和 端口
										//如果是服务器，保存的是连接的客户端的 ip 地址

	SOCKETDATA m_sSocketData;			//发送数据的主体部分，动态扩展，如果发送的数据过大，自动的扩展空间
										//初始空间为10240，模仿老师的空间大小吧

	int m_iPort;						//如果本地绑定为服务器，服务器的端口号
	bool m_fConnected;					//保存当前是否连接到了指定的服务器，本变量仅仅针对客户端，对于服务器并没有
										//连接是否成功的标志作用
	bool m_fIsServer;					//保存是否是服务器，因为服务器和客户端的代码稍有不同
	SocketState m_sckState;				//socket 暂时的状态

	//==============================================================================================
	//此处为了进一步的方便使用，此处直接接收的内容也不用用户定义了，直接就可以通过类接口访问接收到的信息
	MSGHEAD m_msgHead;			//如果包头接收正确，这个变量保存的是包体的长度
	char* m_pDataReceive;				//用于指向接收数据的空间首地址

protected:

};
