#include "BForm.h"
#include "main.h"

//用于保存消息的类型的枚举
enum MSGTYPE
{
	eMsgNone = 0,				//没啥用，就是写一个没用的，用于初始化
	eMsgConnect = 1,			//客户端连接后发送自己进入房间的信息
	eMsgConfirmResp = 2,		//客户端连接的信息处理结果
	eMsgReady = 3,				//客户端确定后是否准备完毕，全部准备完毕开始游戏
	eMsgStartResp = 4,			//表示游戏开始了，显示主游戏窗口内容
	eMsgActSend = 5,			//客户端发送的自己的飞机执行动作
	eMsgActSendResp = 6,		//客户端收到的自己的执行动作被成功处理的消息
	eMsgActGet = 7,				//客户端获得自己在服务器的消息队列的信息，循环发送直到没有消息
	eMsgActGetResp = 8,			//客户端获得的自己消息队列的信息
	eMsgGameEnd = 9,			//游戏结束，客户端可以自行做出其他选择内容
	eMsgPaint = 10				//表示需要同步刷新对应的绘图内容了
};

//这个消息内容是包括了客户端在最近时间内可能进行的动作内容
struct MSG_CONNECT
{
	EPlaneType eType;		//表示的是飞机的种类
};

//这个消息内容是包括了客户端在最近时间内可能进行的动作内容
struct MSG_CONFIRMRESP
{
	BYTE bPlaneID;				//主要用于表示飞机的 ID ，由服务器统一分配，客户端不得更改
	EPlaneType eType;		//表示的是飞机的种类
	SPlane sPlane;				//此处为了节省带宽，仅仅发送飞机的属性值，防止用户作弊
};

//设定飞机的初始化信息
//这个消息内容是包括了客户端在最近时间内可能进行的动作内容
struct MSG_STARTRESP
{
	BYTE bPlaneID;				//主要用于表示飞机的 ID ，由服务器统一分配，客户端不得更改
	POINT pPos;					//飞机的初始化位置信息
	float fAngle;				//飞机初始化的朝向信息
};

//这个消息内容是包括了客户端在最近时间内可能进行的动作内容
struct MSG_ACTSEND
{
	BYTE bPlaneID;				//主要用于表示飞机的 ID ，由服务器统一分配，客户端不得更改
	EPlanAction eAction;		//飞机的动作种类，主要用于更新内容
	int iFrameNum;				//帧数标号，防止网速过慢的用户拖慢整体的游戏进度
};

struct MSG_ACTSENDRESP
{
	BYTE bPlaneID;				//主要用于表示飞机的 ID ，由服务器统一分配，客户端不得更改
	EPlanAction eAction;		//飞机的动作种类，主要用于更新内容
	bool fUpdate;				//帧数标号，防止网速过慢的用户拖慢整体的游戏进度
};

struct MSG_GETRESP
{
	BYTE bPlaneID;				//主要用于表示飞机的 ID ，由服务器统一分配，客户端不得更改
	EPlanAction eAction;		//表示该动作的类型
	int iFrameNum;				//表示该动作的帧数
};
