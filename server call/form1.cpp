#include "main.h"
#include "BSocket.h"
#include "MsgCall.h"
#include <map>
using namespace std;

CBSocket m_sSocketServer;					//本地的一个socket，用于服务器的使用

//便于通过 id 找到对应的 socket
map<int, SSocketOpt>m_mapClients;	

void form1_Load()
{
	//创建一个绑定本地地址的socket，端口为 4444 
	m_sSocketServer.OnCreate(form1.hWnd());
	if (m_sSocketServer.OnBind(4444))
	{
		if(m_sSocketServer.OnListen())
			form1.TextSet(TEXT("服务器监听成功"));
	}
}
void OnAccept(SOCKET iSocket)
{	
	static int siClientID = 0;
	//接收到连接的请求，调用类函数接受连接，然后接收客户端发来的登陆信息
	if (!m_sSocketServer.OnAccept())
	{
		MsgBox(TEXT("连接失败，请重试！"));
	}
	else
	{
		form1.Control(ID_txtServerMain).SelTextSet(StrAppend(TEXT("服务器已经成功和客户端建立连接！当前客户端数量："),Str(m_sSocketServer.ClientCount()),TEXT("\r\n")));
		
		int clientcount = m_sSocketServer.ClientCount();
		//为客户端赋值 id 
		m_sSocketServer.OnSend((char*)&siClientID, sizeof(int), eMsgClientID,
			m_sSocketServer.GetSocket(clientcount - 1));

		form1.Control(ID_txtServerMain).SelTextSet(StrAppend(L"新增用户ID为：", Str(siClientID),
			L"\t新增用户的连接socket为：", Str(int(m_sSocketServer.GetSocket(clientcount - 1))),
			L"\r\n"));

		SClientIDChange clientchange;
		//首先向该用户广播所有可用的 id ，通过 map 来遍历所有的在线用户
		map<int, SSocketOpt>::iterator it = m_mapClients.begin();
		SOCKET skt = m_sSocketServer.GetSocket(clientcount - 1);
		for (; it != m_mapClients.end(); it++)
		{
			clientchange.fAddorSub = true;
			clientchange.iChangeID = it->first;
			m_sSocketServer.OnSend((char*)&clientchange, sizeof(SClientIDChange), eMsgClientIDChange,
				skt);
		}

		//然后向所有的客户端广播新用户在线状态
		clientchange.fAddorSub = true;
		clientchange.iChangeID = siClientID;
		for (int i = 0; i < clientcount - 1; i++)
		{
			m_sSocketServer.OnSend((char*)&clientchange, sizeof(SClientIDChange), eMsgClientIDChange,
				m_sSocketServer.GetSocket(i));
		}
		//更新服务器客户端的数据存储内容
		SSocketOpt state;
		state.sSocketDst = NULL;
		state.sSocketLaunch = skt;		//这里应该是连接的那个 socket 的句柄内容
		m_mapClients.insert(pair<int, SSocketOpt>(siClientID, state));

		siClientID++;		//每个人的 id 不断增加保证不会重复
	}
}
void OnClose(SOCKET pSock)
{
	//关闭连接，并且显示提示信息
	m_sSocketServer.OnClose(pSock);
	form1.Control(ID_txtServerMain).SelTextSet(StrAppend(TEXT("连接中断！当前连接数量："),Str(m_sSocketServer.ClientCount()), TEXT("\r\n")));
	
}
void OnReceive(SOCKET pSock)
{
	//首先定位光标选中内容的位置
	form1.Control(ID_txtServerMain).SelSet(-1,-1);

	//接收消息
	char* sData;
	MSGHEAD msgHead;
	//如果由于网速问题接收失败的话，不进行下面的内容的判断
	if (!m_sSocketServer.OnReceive(&sData, &msgHead, pSock))
		return;

	switch (msgHead.dwcmdID)
	{
	case eMsgCallRequest:
	{
		//将消息内容发送到对应的socket那里，然后等待回音
		SCallRequest* callrequest = (SCallRequest*)sData;

		form1.Control(ID_txtServerMain).SelTextSet(
			StrAppend(L"呼叫请求：发起者ID：", Str(callrequest->iLaunchID),
			L"\t接收者ID：", Str(callrequest->iReceiveID), L"\r\n"));

		map<int, SSocketOpt>::iterator it = m_mapClients.find(callrequest->iReceiveID);
		if (it != m_mapClients.end())
		{
			if (it->second.sSocketDst)
			{
				form1.Control(ID_txtServerMain).SelTextSet(
					StrAppend(L"呼叫请求回应：发起者ID：", Str(callrequest->iLaunchID),
						L"\t接收者ID：", Str(callrequest->iReceiveID), L"\t忙线中，拒绝。\r\n"));
				//表示该用户正在通话中，此处你不应该给他打电话
				SRequestBack requestback;
				requestback.fRequestBack = false;
				requestback.iLaunchID = callrequest->iLaunchID;
				requestback.iReceiveID = callrequest->iReceiveID;
				m_sSocketServer.OnSend(sData, msgHead.iDataLength, eMsgRequestBack, pSock);
			}
			else
				m_sSocketServer.OnSend(sData, sizeof(SCallRequest), eMsgCallRequest,
					it->second.sSocketLaunch);
		}
	}
		break;
	case eMsgRequestBack:
	{
		SRequestBack* requestback = (SRequestBack*)sData;

		SOCKET skt = NULL;
		form1.Control(ID_txtServerMain).SelTextSet(
			StrAppend(L"呼叫请求回应：发起者ID：", Str(requestback->iLaunchID),
				L"\t接收者ID：", Str(requestback->iReceiveID),
				requestback->fRequestBack ? L"\t同意语音\r\n" : L"\t拒绝语音\r\n"));
		//如果对方接收内容，服务器就要保存好两个人的匹配信息
		if (requestback->fRequestBack)
		{
			//首先找到对应的要呼叫的对方的 socket
			skt = m_mapClients.find(requestback->iReceiveID)->second.sSocketLaunch;
			m_mapClients.find(requestback->iLaunchID)->second.sSocketDst = skt;
			skt = m_mapClients.find(requestback->iLaunchID)->second.sSocketLaunch;
			m_mapClients.find(requestback->iReceiveID)->second.sSocketDst = skt;
		}
		//然后向回馈的那个人发送自己的接听欲望
		m_sSocketServer.OnSend(sData, msgHead.iDataLength, eMsgRequestBack,
			m_mapClients.find(requestback->iReceiveID)->second.sSocketLaunch);
	}
	break;
	case eMsgSounds:
	{
		SSounds* sound = (SSounds*)sData;
		//找到对应的对象的 socket 句柄，然后发送给他
		map<int, SSocketOpt>::iterator it = m_mapClients.find(sound->iReceiveID);
		if (it != m_mapClients.end())
		{
			m_sSocketServer.OnSend(sData, msgHead.iDataLength, eMsgSounds, 
				it->second.sSocketLaunch);

			form1.Control(ID_txtServerMain).SelTextSet(
				StrAppend(L"语音消息交换：发起者socket：", Str(it->second.sSocketLaunch),
					L"\t接收者socket：", Str(int(it->second.sSocketDst)), L"\r\n"));
		}
	}
	break;
	case eMsgHangUp:
	{
		map<int, SSocketOpt>::iterator it = m_mapClients.begin();
		for (; it != m_mapClients.end(); it++)
		{
			if (it->second.sSocketLaunch == pSock)
			{
				//找到连接的对象，然后发给他，你对象挂断了电话
				m_sSocketServer.OnSend(NULL, 0, eMsgHangUp, it->second.sSocketDst);
				form1.Control(ID_txtServerMain).SelTextSet(
					StrAppend(L"用户挂断：发起者socket：", Str(it->second.sSocketLaunch),
						L"\t接收者socket：", Str(int(it->second.sSocketDst)), L"\r\n"));
				it->second.sSocketDst = NULL;
				break;
			}
		}
		it = m_mapClients.begin();
		//清除和他相连的句柄的目标信息
		for (; it != m_mapClients.end(); it++)
		{
			if (it->second.sSocketDst == pSock)
			{
				//找到连接的对象，然后发给他，你对象挂断了电话
				it->second.sSocketDst = NULL;
				break;
			}
		}
	}
		break;
	default:
		break;
	}

}
void form1_NetWorkEvent(int iSocket, int iEvent, int iError)
{
	m_sSocketServer.SocketEventsGenerator(iSocket, iEvent, iError);
	//得到发出此事件的客户端套接字
	SOCKET pSock =(SOCKET)iSocket;
	switch(iEvent)
	{
	case FD_ACCEPT:		//客户端连接请求
		{
			OnAccept(iSocket);
			break;
		}
	case FD_CLOSE:		//户端事件:
		{
			OnClose(pSock);
			map<int, SSocketOpt>::iterator it = m_mapClients.begin();
			int idClose = 0;
			for (;it!=m_mapClients.end();it++)
			{
				if (it->second.sSocketLaunch == pSock)
				{
					idClose = it->first;
					//找到连接的对象，然后发给他，你对象挂断了电话
					m_sSocketServer.OnSend(NULL, 0, eMsgHangUp, it->second.sSocketDst);
					form1.Control(ID_txtServerMain).SelTextSet(
						StrAppend(L"用户socket被关闭：发起者socket：", Str(it->second.sSocketLaunch),
							L"\t接收者socket：", Str(int(it->second.sSocketDst)), L"\r\n"));
					m_mapClients.erase(it);		//清除该内容
					break;
				}
			}
			it = m_mapClients.begin();
			//清除和他相连的句柄的目标信息
			for (; it != m_mapClients.end(); it++)
			{
				if (it->second.sSocketDst == pSock)
				{
					//找到连接的对象，然后发给他，你对象挂断了电话
					it->second.sSocketDst = NULL;
					break;
				}
			}

			//针对关闭事件，除了清除 map ，还需要通知客户端下拉列表的删减
			SClientIDChange clientchange;
			clientchange.fAddorSub = false;
			clientchange.iChangeID = idClose;
			for (int i = 0; i < m_sSocketServer.ClientCount(); i++)
			{
				m_sSocketServer.OnSend((char*)&clientchange, sizeof(SClientIDChange), eMsgClientIDChange,
					m_sSocketServer.GetSocket(i));
			}
			break;
		}
	case FD_READ://网络数据包到达事件
		{
			OnReceive(pSock);
			break;
		}
	default:
		break;
	}
}
void form1_Unload()
{
	m_sSocketServer.OnClose();
}
void cmdOpenSocket_Click()
{
	m_sSocketServer.OnCreate(form1.hWnd());
	m_sSocketServer.OnBind(4444);
	m_sSocketServer.OnListen();
	form1.Control(ID_txtServerMain).SelTextSet(TEXT("打开服务器。。。\r\n"));
	form1.Control(ID_cmdOpenSocket).EnabledSet(false);
	form1.Control(ID_cmdCloseSocket).EnabledSet(true);
}
void cmdCloseSocket_Click()
{
	m_sSocketServer.OnClose(m_sSocketServer.GetSocket(0));
	form1.Control(ID_txtServerMain).SelTextSet(TEXT("服务器已关闭。。。\r\n"));
	form1.Control(ID_cmdOpenSocket).EnabledSet(true);
	form1.Control(ID_cmdCloseSocket).EnabledSet(false);
	
}
void form1_EventsMap()
{
	form1.EventAdd(0, eForm_Load, form1_Load);
	form1.EventAdd(0, eForm_Unload, form1_Unload);
	form1.EventAdd(0, eNetWorkEvent, form1_NetWorkEvent);
	form1.EventAdd(ID_cmdOpenSocket, eCommandButton_Click, cmdOpenSocket_Click);
	form1.EventAdd(ID_cmdCloseSocket, eCommandButton_Click, cmdCloseSocket_Click);

	form1.IconSet(IDI_ICON1);
	form1.Show();
}
