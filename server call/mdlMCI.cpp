//////////////////////////////////////////////////////////////////////////
// mdlMCI.cpp：实现 MCI 媒体控制
// 发送 MCI 命令、从命令返回、及出错信息返回
//
//////////////////////////////////////////////////////////////////////////

#include "mdlMCI.h"

bool MciSend(tstring &sCmd, tstring &sError)
{
	DWORD ret = mciSendString(sCmd.c_str(), NULL, 0, NULL);
	if (0 == ret)
	{
		// 发送命令成功：返回 true
		return true;
	}
	else
	{
		// 发送命令失败：返回 false 并且通过 sError 获得失败信息
		// 调用 MciGetErrorStr() 获得错误的提示信息，修改引用的形参 sError 的值
		sError = MciGetErrorStr(ret);
		return false;
	}

}

bool MciSend(LPTSTR szCmd)
{
	DWORD ret = mciSendString(szCmd, NULL, 0, NULL);
	return (0==ret);
}

tstring MciSendRetStr(tstring &sCmd, tstring &sError)
{
	tstring sResult(TEXT(""));	//结果字符串
	TCHAR szBuff[1024]={0};		//用于接收字符串的缓冲区并且清零
								//仅用于接收临时字符串，接收到字符串后，从此缓冲区拷贝到
								//所需要的空间中（如：sResult中），函数结束，此数组内容全部释放清空
	DWORD ret = 
		mciSendString( sCmd.c_str(), szBuff, sizeof(szBuff)/sizeof(TCHAR), NULL);
	//此处的 sizeof(szBuff)/sizeof(TCHAR) 指的是 szBuff所能保存的字符数，即 1024 

	if (0 == ret)
	{
		// 命令发送成功，将 szBuff[] 中的内容拷贝到 是 Result 并返回
		sResult = szBuff;
	}
	else
	{
		// 命令发送失败：返回空的字符串并且将错误信息由 sError 返回
		sResult = TEXT("");
		//调用函数 MciGetErrorStr() 获得错误提示信息
		sError = MciGetErrorStr(ret);

	}
	return sResult;		//如果成功打开，返回的是媒体的长度

}

tstring MciGetErrorStr(DWORD fdwError)
{
	tstring sError(TEXT(""));
	TCHAR szBuff[1024]={0};		//用于接收错误字符串的信息的缓冲区
								//仅用于接收临时字符串，接收到字符串后，从此缓冲区拷贝到
								//所需要的空间中（如：sResult中），函数结束，此数组内容全部释
	if ( mciGetErrorString(fdwError, szBuff, 1024))
	{
		sError= szBuff;
	}
	else
		sError=TEXT("");

	return sError;
}

//////////////////////////////////////////////////////////////////////////
// CBWindMCI 类的实现
// 主要用于 mci 命令封装，更加方便调用
//
// Programmed by Zhizhi
// Last Modified: 2019-5-7
//////////////////////////////////////////////////////////////////////////


CBWindMCI::CBWindMCI( tstring sFileName )
{
	//全部执行一个函数，便于通用性
	TCHAR sFile[1024];
	_tcscpy(sFile, sFileName.c_str());

	CBWindMCI((LPTSTR)sFile);
}

CBWindMCI::CBWindMCI( LPTSTR sFileName )
{
	//调用打开的函数
	OpenMedia(sFileName);

	//变量初始化
	*m_sError = TEXT('\0');
	*m_sMediaName = TEXT('\0');
	iMediaName++;
}

CBWindMCI::CBWindMCI()
{
	CBWindMCI(TEXT(""));
}

CBWindMCI::~CBWindMCI()
{
	CloseMedia();
}

bool CBWindMCI::OpenMedia( tstring sFileName, bool fVolumeAdjust /*= true*/ )
{
	// 如果是文件名为非空
	if(*m_sMediaName != TEXT('\0'))	
	{
		// 如果用户已经打开了一个媒体，那么就关闭原有的媒体，然后重新打开新的媒体
		CloseMedia();
		return OpenMedia(sFileName, fVolumeAdjust);
	}

	tstring sCommand(TEXT("")), sError(m_sError);

	sCommand += TEXT("open \"");
	sCommand += sFileName;
	//此处通过变量 fVolumeAdjust 来控制后面拼接的字符串内容
	sCommand += fVolumeAdjust ? StrAppend(TEXT("\" type mpegvideo alias "),Str(iMediaName)) : StrAppend(TEXT("\" alias "),Str(iMediaName));	// type mpegvideo 为了能够控制播放音量
								
	// 发送命令
	bool r = MciSend(sCommand, sError);
	if (!r)
	{
		// 如果命令发送失败，那么直接拷贝错误信息到字符串
		// 然后返回失败，否则失败信息清空
		_tcscpy(m_sError, sError.c_str());
		return false;
	}
	//如果命令发送成功，那么更新错误信息为 TEXT("")
	// 更新错误信息，然后更新文件名
	*m_sError = TEXT('\0');
	_tcscpy(m_sMediaName, sFileName.c_str());

	// 如果命令发送成功，那么开始设置媒体的格式
	// 设置媒体的时间格式：毫秒 (ms)
	MciSend(StrAppend(TEXT("set "),Str(iMediaName),TEXT(" Time Format ms")));
			
	return true;
}

bool CBWindMCI::OpenMedia( LPTSTR sFileName, bool fVolumeAdjust /*= true*/ )
{
	//全部调用上面的 tstring 的函数
	tstring sFile = sFileName;		//这里全部转换成 tstring 的变量

	return OpenMedia(sFile, fVolumeAdjust);
}

void CBWindMCI::CloseMedia()
{
	//如果当前没有打开媒体文件，直接返回
	if(*m_sMediaName == TEXT('\0'))	return;

	//先暂停媒体播放，然后关闭媒体
	PalseMedia();							//先停止播放媒体	
	MciSend(StrAppend(TEXT("close "),Str(iMediaName)));				//关闭媒体
			
	///清空文件名变量
	*m_sError = TEXT('\0');
	*m_sMediaName = TEXT('\0');
}

LPTSTR CBWindMCI::GetMediaLastError()
{
	return m_sError;
}

void CBWindMCI::PlayMedia(tstring sFileName /*= NULL*/ )
{
	//如果用户在这里指定了播放的媒体文件是什么，那么要切换当前的播放媒体文件内容
	if (sFileName != TEXT(""))
	{
		//如果当前已经打开媒体文件，先关闭当前媒体文件
		if(*m_sMediaName != TEXT('\0'))
			CloseMedia();

		//当前播放媒体文件此时一定为空，然后
		//先打开，然后从 iStartPos 位置播放，
		//如果打开失败，直接返回
		if (OpenMedia(sFileName))
		{
			PlayMedia();

			return;
		}
		return;
	}
	
	//从当前位置播放
	//如果打开成功，改变标志变量
	if(MciSend(StrAppend(TEXT("play "),Str(iMediaName))))		//发送播放命令
		m_fPlay = true;
}

int CBWindMCI::MediaLength()
{
	//如果当前没有打开媒体文件，直接返回
	if(*m_sMediaName == TEXT('\0'))	return -1;

	//发送命令
	tstring sCommand(TEXT("")), sError(TEXT("")), sRet(TEXT(""));
	sCommand = StrAppend(TEXT("status "),Str(iMediaName),TEXT(" length"));	
	sError = TEXT("");
	sRet = MciSendRetStr(sCommand, sError);

	if (sRet == TEXT(""))
	{
		//如果执行失败，那么返回 -1
		//更新错误信息
		_tcscpy(m_sError, sError.c_str());
		return -1;
	}

	return (int)Val(sRet.c_str());
}

void CBWindMCI::PalseMedia()
{
	//如果当前没有打开媒体文件，直接返回
	if(*m_sMediaName == TEXT('\0'))	return;
	
	//如果此时打开了媒体，那么暂停
	//如果打开成功，改变标志变量
	if(MciSend(StrAppend(TEXT("pause "),Str(iMediaName))))
		m_fPlay = false;

}

int CBWindMCI::PlayPos()
{
	//如果当前没有打开媒体文件，直接返回
	if(*m_sMediaName == TEXT('\0'))	return -1;

	//获得当前的播放媒体的位置
	tstring sCommand(TEXT("")), sError(TEXT("")), sRet(TEXT(""));
	sCommand = StrAppend(TEXT("status "),Str(iMediaName),TEXT(" position"));
	sError = TEXT("");
	sRet = MciSendRetStr(sCommand, sError);

	//如果执行失败，那么将现有的错误信息更新
	if (sRet == TEXT(""))
	{
		//如果执行失败，那么返回 -1
		//更新错误信息
		_tcscpy(m_sError, sError.c_str());
		return -1;
	}
	//如果正确执行，那么更新错误信息的状态
	*m_sError = TEXT('\0');
	return (int)Val(sRet.c_str());
}

int CBWindMCI::Volume()
{
	//如果当前没有打开媒体文件，直接返回
	if(*m_sMediaName == TEXT('\0'))	return -1;

	//获得当前的播放媒体的声音大小
	//声音的范围是：0-1000
	tstring sCommand(TEXT("")), sError(TEXT("")), sRet(TEXT(""));

	//获得当前的播放媒体的位置
	sCommand = StrAppend(TEXT("status "),Str(iMediaName),TEXT(" volume"));
	sError = TEXT("");
	sRet = MciSendRetStr(sCommand, sError);

	//如果执行失败，那么将现有的错误信息更新
	if (sRet == TEXT(""))
	{
		//如果执行失败，那么返回 -1
		//更新错误信息
		_tcscpy(m_sError, sError.c_str());
		return -1;
	}
	//如果正确执行，那么更新错误信息的状态
	*m_sError = TEXT('\0');
	return (int)Val(sRet.c_str());
}

bool CBWindMCI::VolumeSet( int iVolume )
{
	//如果当前没有打开媒体文件，直接返回
	if(*m_sMediaName == TEXT('\0'))	return false;

	//设置当前的播放声音的大小
	//声音的范围是：0-1000
	tstring sCommand(TEXT("")), sError(TEXT("")), sRet(TEXT(""));

	//获得当前的播放媒体的位置
	sCommand = StrAppend(TEXT("setaudio "),Str(iMediaName),TEXT(" volume to "));
	sCommand += Str(iVolume);
	sError = TEXT("");
	sRet = MciSendRetStr(sCommand, sError);

	//如果执行失败，那么将现有的错误信息更新
	if (sRet == TEXT(""))
	{
		//如果执行失败，那么返回 -1
		//更新错误信息
		_tcscpy(m_sError, sError.c_str());
		return false;
	}
	//如果正确执行，那么更新错误信息的状态
	*m_sError = TEXT('\0');
	return true;
}

LPTSTR CBWindMCI::GetMediaID()
{
	//返回当前媒体在 MCI 中的名字
	//增加类的可扩展性

	return Str(iMediaName);
}

void CBWindMCI::PlayPosSet( int iPos )
{
	//如果当前没有打开媒体文件，直接返回
	if(*m_sMediaName == TEXT('\0'))	return;

	if(iPos>MediaLength() || iPos<0)	return;

	//如果此时打开了媒体文件，设置媒体的位置
	//如果此时正在播放，设置后自动播放
	//如果此时暂停，仅仅改变媒体位置，不改变播放状态
	if (m_fPlay)
	{
		PalseMedia();
		MciSend(StrAppend(TEXT("seek "),Str(iMediaName),TEXT(" to "),Str(iPos)));
		PlayMedia();
	}
	else
		MciSend(StrAppend(TEXT("seek "),Str(iMediaName),TEXT(" to "),Str(iPos)));
	

}

void CBWindMCI::StopMedia()
{
	//如果当前没有打开媒体文件，直接返回
	if(*m_sMediaName == TEXT('\0'))	return;

	//如果此时打开了媒体，那么暂停
	//如果打开成功，改变标志变量
	if(MciSend(StrAppend(TEXT("stop "),Str(iMediaName))))
		m_fPlay = false;
}

int CBWindMCI::iMediaName = 0;

