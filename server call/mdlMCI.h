//////////////////////////////////////////////////////////////////////////
// mdlMCI.h: MCI 媒体控制声明
// 发送 MCI 命令、从命令返回和出错信息返回
// 
//////////////////////////////////////////////////////////////////////////

// 支持：需要 BWindows.h, 以包含 windows.h , 使用 tstring
#include "BWindows.h"

// MciSend: 发送一个 mci 命令，不返回命令发送的信息字符串
// 成功返回 true, 失败返回 false
// 失败时候，还从 sError 返回具体的错误提示字符串
// 参数中的 & 表示引用传递
bool MciSend(tstring &sCmd, tstring &sError);
// 重载版本（支持以字符串指针命令传递命令字符串，但不支持返回错误信息）
bool MciSend(LPTSTR szCmd);

// MciSendRetStr: 发送一个 mci 命令，返回命令所返回的信息字符串
// 失败时返回空字符串，还从 sError 返回具体的错误提示字符串
tstring MciSendRetStr(tstring &sCmd, tstring &sError);

// GetMciErrorStr：根据一个错误代码（fdwError）获得对应的 mci 命令失败原因的说明字符串
// fdwError 为 mciSendCommand 或者 mciSendString 函数所返回的错误代码
tstring MciGetErrorStr(DWORD fdwError);

//////////////////////////////////////////////////////////////////////////
// CBWindMCI 类的声明
// 主要用于 mci 命令封装，更加方便调用
//
// Programmed by Zhizhi
// Last Modified: 2019-5-7
//////////////////////////////////////////////////////////////////////////

class CBWindMCI
{
public:
	// =======================================================================
	// 类公有成员和成员函数
	//==================================================================================

	// 构造函数
	// 如果已经给出了要打开的媒体的路径直接打开媒体，不需要用户再次执行打开媒体的函数
	// 为了函数的通用性，构造函数由两个重载版本构成，tstring 和 LPTSTR
	CBWindMCI();
	CBWindMCI(tstring sFileName);
	CBWindMCI(LPTSTR sFileName);

	// 析构函数
	//如果用户此时没有关闭媒体，自动关闭媒体，释放资源
	~CBWindMCI();

	//打开媒体
	//仅仅打开媒体，并不开始播放媒体
	// 为了函数的通用性，本函数由两个重载版本构成，tstring 和 LPTSTR
	bool OpenMedia(tstring sFileName, bool fVolumeAdjust = true);
	bool OpenMedia(LPTSTR sFileName, bool fVolumeAdjust = true);

	//关闭媒体，释放资源
	void CloseMedia();

	//获得媒体的长度，返回值为字符串的首地址吧，这样返回值什么变量都能够接收到
	//注意此处的赶回的媒体长度的最小单位为 ms 
	int MediaLength();

	//播放媒体，默认从 0 开始
	//用户可以通过改变第一个参数来改变播放位置
	//在播放中途可以改变当前的媒体的播放位置
	void PlayMedia(tstring sFileName = TEXT(""));

	//停止播放媒体，
	//回到最初的位置
	void PalseMedia();

	//从头播放，停止播放媒体
	void StopMedia();

	//设置和获得播放的位置
	//保留当前的播放状态，如果播放，改变后继续播放，如果，暂停，改变后暂停
	void PlayPosSet(int iPos);
	int PlayPos();

	//获得和设置当前的音量大小
	int Volume();
	bool VolumeSet(int iVolume);

	//为了增加类的扩展性，此处支持返回当前媒体在 MCI 中的名称
	LPTSTR GetMediaID();

	//获得最新的出错信息
	LPTSTR GetMediaLastError();

private:
	//此处学会了，真正了解了这个 TCHAR 和 LPTSTR 的用法
	//不错，挺好的

	TCHAR m_sError[1024];	//用于保存出错信息，为了返回指针增加函数返回值的通用性
						//但是，如果用指针会无法传递回正确的内容，所以定义此类内的全局变量

	TCHAR m_sMediaName[2048];	// 存储暂时打开的媒体文件名，如果已经打开，那么删除原来的，自动打开另一个

	bool m_fPlay;
	//用于动态保存当前打开的媒体的名称
	static int iMediaName;
};