#include "CBSound.h"




CBSound::CBSound()
{
	m_hWaveIn = NULL;
	m_hWaveOut = NULL;
	m_hWnd = NULL;

	//指定默认的录音格式
	m_soundFormat.wFormatTag = WAVE_FORMAT_PCM;
	m_soundFormat.nChannels = 1;
	m_soundFormat.nSamplesPerSec = 11025;
	m_soundFormat.nAvgBytesPerSec = 11025;
	m_soundFormat.wBitsPerSample = 8;
	m_soundFormat.cbSize = 0;
}

CBSound::~CBSound()
{
	CloseWaveDevs();
}

int CBSound::GetWaveInDevNum()
{
	return waveInGetNumDevs();
}

tstring CBSound::GetWaveInDevName(DWORD dwID)
{
	if (dwID >= waveInGetNumDevs())
		return L"";

	tstring sDevText = L"";
	WAVEINCAPS waveCaps;
	int res = waveInGetDevCaps(dwID, &waveCaps, sizeof(WAVEINCAPS));
	if (res == MMSYSERR_NOERROR)
	{
		sDevText = waveCaps.szPname;
	}
	return sDevText;
}

void CBSound::SetRecordFormat(EWAVE_FORMAT eFormat, DWORD dwChannels /*= 1*/, DWORD dwSamplesPerSec /*= 110250*/, DWORD dwBytesPerSample /*= 8*/ )
{
	switch (eFormat)
	{
	case eWAVE_FORMAT_1M08:
	{
		m_soundFormat.nChannels = 1;
		m_soundFormat.nSamplesPerSec = 11025;
		m_soundFormat.nAvgBytesPerSec = 11025;
		m_soundFormat.wBitsPerSample = 8;
	}
		break;
	case eWAVE_FORMAT_1M16:
	{
		m_soundFormat.nChannels = 1;
		m_soundFormat.nSamplesPerSec = 11025;
		m_soundFormat.nAvgBytesPerSec = 11025;
		m_soundFormat.wBitsPerSample = 16;
	}
		break;
	case eWAVE_FORMAT_1S08:
	{
		m_soundFormat.nChannels = 2;
		m_soundFormat.nSamplesPerSec = 11025;
		m_soundFormat.nAvgBytesPerSec = 11025;
		m_soundFormat.wBitsPerSample = 8;
	}
		break;
	case eWAVE_FORMAT_1S16:
	{
		m_soundFormat.nChannels = 2;
		m_soundFormat.nSamplesPerSec = 11025;
		m_soundFormat.nAvgBytesPerSec = 11025;
		m_soundFormat.wBitsPerSample = 16;
	}
		break;
	case eWAVE_FORMAT_2M08:
	{
		m_soundFormat.nChannels = 1;
		m_soundFormat.nSamplesPerSec = 22050;
		m_soundFormat.nAvgBytesPerSec = 22050;
		m_soundFormat.wBitsPerSample = 8;
	}
	break;
	case eWAVE_FORMAT_2M16:
	{
		m_soundFormat.nChannels = 1;
		m_soundFormat.nSamplesPerSec = 22050;
		m_soundFormat.nAvgBytesPerSec = 22050;
		m_soundFormat.wBitsPerSample = 16;
	}
	break;
	case eWAVE_FORMAT_2S08:
	{
		m_soundFormat.nChannels = 2;
		m_soundFormat.nSamplesPerSec = 22050;
		m_soundFormat.nAvgBytesPerSec = 22050;
		m_soundFormat.wBitsPerSample = 8;
	}
	break;
	case eWAVE_FORMAT_2S16:
	{
		m_soundFormat.nChannels = 2;
		m_soundFormat.nSamplesPerSec = 22050;
		m_soundFormat.nAvgBytesPerSec = 22050;
		m_soundFormat.wBitsPerSample = 16;
	}
	break;
	case eWAVE_FORMAT_4M08:
	{
		m_soundFormat.nChannels = 1;
		m_soundFormat.nSamplesPerSec = 44100;
		m_soundFormat.nAvgBytesPerSec = 44100;
		m_soundFormat.wBitsPerSample = 8;
	}
	break;
	case eWAVE_FORMAT_4M16:
	{
		m_soundFormat.nChannels = 1;
		m_soundFormat.nSamplesPerSec = 44100;
		m_soundFormat.nAvgBytesPerSec = 44100;
		m_soundFormat.wBitsPerSample = 16;
	}
	break;
	case eWAVE_FORMAT_4S08:
	{
		m_soundFormat.nChannels = 2;
		m_soundFormat.nSamplesPerSec = 44100;
		m_soundFormat.nAvgBytesPerSec = 44100;
		m_soundFormat.wBitsPerSample = 8;
	}
	break;
	case eWAVE_FORMAT_4S16:
	{
		m_soundFormat.nChannels = 2;
		m_soundFormat.nSamplesPerSec = 44100;
		m_soundFormat.nAvgBytesPerSec = 44100;
		m_soundFormat.wBitsPerSample = 16;
	}
	break;
	default:
	{
		m_soundFormat.nChannels = (WORD)dwChannels;
		m_soundFormat.nSamplesPerSec = dwSamplesPerSec;
		m_soundFormat.nAvgBytesPerSec = dwSamplesPerSec;
		m_soundFormat.wBitsPerSample = (WORD)dwBytesPerSample;
	}
		break;
	}

	//如果此时音频设备处于打开状态，关闭并重新打开，更新设置内容
	WaveInit(m_hWnd);
}

bool CBSound::WaveInit(HWND hWnd, int iWaveInDevID /*= -1*/, int iWaveOutDevID /*= -1*/ )
{
	if (m_hWaveIn || m_hWaveOut)	//如果已经开启设备，关闭重新开启
		CloseWaveDevs();

	if (waveInGetNumDevs() == 0 || waveOutGetNumDevs() == 0)
		return false;

	if (!hWnd)		//设置窗口的句柄
		return false;
	else
		m_hWnd = hWnd;

	if (iWaveInDevID == -1 || iWaveInDevID >= (int)waveInGetNumDevs())	//对设备 ID 进行把关
		iWaveInDevID = WAVE_MAPPER;
	if (iWaveOutDevID == -1 || iWaveOutDevID >= (int)waveOutGetNumDevs())	//对设备 ID 进行把关
		iWaveOutDevID = WAVE_MAPPER;

	//针对 pcm 格式的音频，计算方法如下：
	m_soundFormat.nBlockAlign = m_soundFormat.nChannels * m_soundFormat.wBitsPerSample / 8;

	//======================== 录音 ==========================
	//打开录音设备，采用窗口方式接收音频消息
	int res = waveInOpen(&m_hWaveIn, iWaveInDevID, &m_soundFormat, (DWORD)hWnd,
		0L, CALLBACK_WINDOW);
	if (res != MMSYSERR_NOERROR)
		return false;
	//准备内存块录音
	m_pWaveHdrIn[0].lpData = m_cBufferIn;
	m_pWaveHdrIn[0].dwBufferLength = MAX_BUFFER_SIZE; 
	m_pWaveHdrIn[0].dwBytesRecorded = 0;
	m_pWaveHdrIn[0].dwFlags = 0;
	res = waveInPrepareHeader(m_hWaveIn, &m_pWaveHdrIn[0], sizeof(WAVEHDR));
	if (res != MMSYSERR_NOERROR)
		return false;
	//增加内存块
	res = waveInAddBuffer(m_hWaveIn, &m_pWaveHdrIn[0], sizeof(WAVEHDR));
	if (res != MMSYSERR_NOERROR)
		return false;

	//======================== 播放 ==========================
	res = waveOutOpen(&m_hWaveOut, iWaveOutDevID, &m_soundFormat, (DWORD)hWnd,
		0L, CALLBACK_WINDOW);
	if (res != MMSYSERR_NOERROR)
		return false;
	//准备内存块播放
	m_pWaveHdrout[0].lpData = m_cBufferout;
	m_pWaveHdrout[0].dwBufferLength = MAX_BUFFER_SIZE;
	m_pWaveHdrout[0].dwBytesRecorded = 0;
	m_pWaveHdrout[0].dwFlags = 0;
	res = waveOutPrepareHeader(m_hWaveOut, &m_pWaveHdrout[0], sizeof(WAVEHDR));
	if (res != MMSYSERR_NOERROR)
		return false;
	//指定数据块到音频播放缓冲区
	res = waveOutWrite(m_hWaveOut, &m_pWaveHdrout[0], sizeof(WAVEHDR));
	if (res != MMSYSERR_NOERROR)
		return false;

	return true;
}

void CBSound::StartRecord()
{
	waveInStart(m_hWaveIn);//停止录音
}

void CBSound::StopRecord()
{
	waveInStop(m_hWaveIn);//停止录音
}

int CBSound::GetWaveOutDevNum()
{
	return waveOutGetNumDevs();
}

tstring CBSound::GetWaveOutDevName(DWORD dwID)
{
	if (dwID >= waveOutGetNumDevs())
		return L"";

	tstring sDevText = L"";
	WAVEOUTCAPS waveCaps;
	int res = waveOutGetDevCaps(dwID, &waveCaps, sizeof(WAVEOUTCAPS));
	if (res == MMSYSERR_NOERROR)
	{
		sDevText = waveCaps.szPname;
	}
	return sDevText;
}

void CBSound::StartPlay(char* pData /*= NULL*/, DWORD dwDataLen /*= 0*/ )
{
	if (!pData)
	{
		pData = m_cBufferIn;
		dwDataLen = MAX_BUFFER_SIZE;
	}
	memcpy(m_cBufferout, pData, dwDataLen);
}

void CBSound::StopPlay()
{
	waveOutReset(m_hWaveOut);
}

bool CBSound::FreeRecordBuffer()
{
	int res = waveInUnprepareHeader(m_hWaveIn, &m_pWaveHdrIn[0], sizeof(WAVEHDR));
	if (res != MMSYSERR_NOERROR)
		return false;
	
	//准备内存块录音
	m_pWaveHdrIn[0].lpData = m_cBufferIn;
	m_pWaveHdrIn[0].dwBufferLength = MAX_BUFFER_SIZE;
	m_pWaveHdrIn[0].dwFlags = 0;
	res = waveInPrepareHeader(m_hWaveIn, &m_pWaveHdrIn[0], sizeof(WAVEHDR));
	if (res != MMSYSERR_NOERROR)
		return false;
	//增加内存块
	res = waveInAddBuffer(m_hWaveIn, &m_pWaveHdrIn[0], sizeof(WAVEHDR));
	if (res != MMSYSERR_NOERROR)
		return false;

	return true;
}

bool CBSound::FreePlayBuffer()
{
	int res = waveOutUnprepareHeader(m_hWaveOut, &m_pWaveHdrout[0], sizeof(WAVEHDR));

	//准备内存块播放
	m_pWaveHdrout[0].lpData = m_cBufferout;
	m_pWaveHdrout[0].dwBufferLength = MAX_BUFFER_SIZE;
	m_pWaveHdrout[0].dwBytesRecorded = 0;
	m_pWaveHdrout[0].dwFlags = 0;
	//准备内存块进行播放
	res = waveOutPrepareHeader(m_hWaveOut, &m_pWaveHdrout[0], sizeof(WAVEHDR));
	if (res != MMSYSERR_NOERROR)
		return false;
	//指定数据块到音频播放缓冲区
	res = waveOutWrite(m_hWaveOut, &m_pWaveHdrout[0], sizeof(WAVEHDR));
	if (res != MMSYSERR_NOERROR)
		return false;

	return true;
}

HWND CBSound::GetCallbackHwnd()
{
	return m_hWnd;
}

void CBSound::SetCallBackHwnd(HWND hWnd)
{
	if (hWnd)
	{
		m_hWnd = hWnd;
		WaveInit(m_hWnd);
	}
}

void CBSound::CloseWaveDevs()
{
	if (m_hWaveIn)
	{
		waveInClose(m_hWaveIn);
		m_hWaveIn = NULL;
	}
	if (m_hWaveOut)
	{
		waveOutClose(m_hWaveOut);
		m_hWaveOut = NULL;
	}
}

char* CBSound::GetWaveInAddr()
{
	return m_cBufferIn;
}
