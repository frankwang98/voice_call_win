#pragma once
#include "BForm.h"
#include <mmsystem.h>

#pragma comment(lib, "winmm.lib")

#define MAX_BUFFER_SIZE 10240	//定义缓冲块

enum EWAVE_FORMAT
{
	eWAVE_FORMAT_NONE = 0,		//默认设置
	eWAVE_FORMAT_1M08 = 1,		// 11.025 kHz, 单通道, 8 - bit
	eWAVE_FORMAT_1M16 = 2,		// 11.025 kHz, 单通道, 16 - bit
	eWAVE_FORMAT_1S08 = 3,		// 11.025 kHz, 双通道, 8 - bit
	eWAVE_FORMAT_1S16 = 4,		//11.025 kHz, 双通道, 16 - bit
	eWAVE_FORMAT_2M08 = 5,		//22.05 kHz, 单通道, 8 - bit
	eWAVE_FORMAT_2M16 = 6,		//22.05 kHz, 单通道, 16 - bit
	eWAVE_FORMAT_2S08 = 7,		// 22.05 kHz, 双通道, 8 - bit
	eWAVE_FORMAT_2S16 = 8,		// 22.05 kHz, 双通道, 16 - bit
	eWAVE_FORMAT_4M08 = 9,		// 44.1 kHz, 单通道, 8 - bit
	eWAVE_FORMAT_4M16 = 10,		// 44.1 kHz, 单通道, 16 - bit
	eWAVE_FORMAT_4S08 = 11,		// 44.1 kHz, 双通道, 8 - bit
	eWAVE_FORMAT_4S16 = 12		// 44.1 kHz, 双通道, 16 - bit
};

class CBSound
{
public:

	//构造函数
	CBSound();

	//析构函数
	~CBSound();

	//类的初始化音频输入输出的函数
	bool WaveInit(HWND hWnd, int iWaveInDevID = -1, int iWaveOutDevID = -1);

	//============================== 音频输入 ===================================

	//获得音频设备输入的数量
	int GetWaveInDevNum();

	//获得音频输入的设备的名称，有效设备 id 需要在0到音频设备数量减一有效
	tstring GetWaveInDevName(DWORD dwID);
	
	//设置录音的格式
	//如果第一个选择了 eWAVE_FORMAT_NONE ，后面的参数才有效，否则按照默认的设置进行设定
	//如果当前设备正处于打开状态，会自动关闭，重新打开使设置生效
	void SetRecordFormat(EWAVE_FORMAT eFormat, DWORD dwChannels = 1, DWORD dwSamplesPerSec = 110250, DWORD dwBytesPerSample = 8);

	//开始录音
	void StartRecord();

	//停止录音
	void StopRecord();	

	//================================= 音频输出 ===============================

	//获得音频设备输入的数量
	int GetWaveOutDevNum();

	//获得音频输入的设备的名称，有效设备 id 需要在0到音频设备数量减一有效
	tstring GetWaveOutDevName(DWORD dwID);

	//通过声音流播放声音
	void StartPlay(char* pData = NULL, DWORD dwDataLen = 0);

	//停止播放声音
	void StopPlay();

	//关闭音频输入输出设备
	void CloseWaveDevs();

	//============================ 其他音频处理函数 ===============================

	//获得对应的音频输入缓冲区地址
	char* GetWaveInAddr();


	//释放录音的buffer
	bool FreeRecordBuffer();

	//释放播放音频的 buffer
	bool FreePlayBuffer();

	//=========================== 其他类信息的获取和更改 =========================
	
	//获得和设置类绑定的窗口句柄
	HWND GetCallbackHwnd();
	void SetCallBackHwnd(HWND hWnd);

	
private:
	HWND m_hWnd;								//录音的消息提醒窗口句柄
	WAVEFORMATEX m_soundFormat;					//类里面保存的用于声音输入和输出的格式
	HWAVEIN m_hWaveIn;							//音频输入的句柄
	HWAVEOUT m_hWaveOut;						//音频输出的句柄
	WAVEHDR m_pWaveHdrIn[3];
	WAVEHDR m_pWaveHdrout[3];
	char m_cBufferIn[MAX_BUFFER_SIZE];
	char m_cBufferout[MAX_BUFFER_SIZE];
};



