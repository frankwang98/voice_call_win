#pragma once

enum EMESSAGE
{
	eMsgNone = 0,					//没有消息
	eMsgCallRequest = 1,			//表示发送来了一个拨打电话的请求
	eMsgSounds = 2,					//表示发送来的是声音信息
	eMsgHangUp = 3,					//表示对方已经挂断
	eMsgRequestBack = 4,			//表示对方是否接听这个语音电话
	eMsgClientID = 5,				//这个指的是客户端的 id, 由服务器统一分配
	eMsgClientIDChange = 6			//表示当前的在线人数发生了改变，需要改变客户端的下拉列表内容
};

struct SCallRequest
{
	int iLaunchID;			//发起的用户 id
	int iReceiveID;			//要拨打的用户 id
};

struct SSounds
{
	int iReceiveID;						//要拨打的用户 id
	char sSoundData[MAX_BUFFER_SIZE];	//表示要传输的内容的缓冲区域
};

struct SRequestBack
{
	int iLaunchID;						//发起的用户 id
	int iReceiveID;						//要拨打的用户 id
	bool fRequestBack;					//表示是否接听
};

struct SClientIDChange
{
	int iChangeID;			//发起的用户 id
	bool fAddorSub;			// true 表示加， false 表示减少
};

struct SSocketOpt
{
	SOCKET sSocketLaunch;			//表示的是对应的 socket 句柄
	SOCKET sSocketDst;				//表示的是通话对象的 socket 句柄
};
