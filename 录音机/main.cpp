#include "BForm.h"
#include "resource.h"
#include "mdlExpand.h"
#include "BSocket.h"
#include "CBSound.h"
#include "MsgCall.h"

CBForm form1(ID_form1);
CBSocket m_sSocket;
CBSound testrecord;

bool m_fIsConnect = false;		//表示是否成功连接了服务器

void cmdConnect_Click();

void form1_Load()
{
	int i;
	//采样设备的搜寻与添加
	for (i=0;i<testrecord.GetWaveInDevNum();i++)
	{
		form1.Control(ID_cboWaveInDevs).AddItem(testrecord.GetWaveInDevName(i));
	}
	form1.Control(ID_cboWaveInDevs).ListIndexSet(1);
	for (i = 0; i < testrecord.GetWaveOutDevNum(); i++)
	{
		form1.Control(ID_cboPlayDevs).AddItem(testrecord.GetWaveOutDevName(i));
	}
	form1.Control(ID_cboPlayDevs).ListIndexSet(1);
	//添加采样设置内容
	{
		CBControl cboWaveInOpt(ID_cboWaveInOpt);
		cboWaveInOpt.AddItem(L"11.025 kHz, 单通道, 8 - bit");
		cboWaveInOpt.AddItem(L"11.025 kHz, 单通道, 16 - bit");
		cboWaveInOpt.AddItem(L"11.025 kHz, 双通道, 8 - bit");
		cboWaveInOpt.AddItem(L"11.025 kHz, 双通道, 16 - bit");
		cboWaveInOpt.AddItem(L"22.05 kHz, 单通道, 8 - bit");
		cboWaveInOpt.AddItem(L"22.05 kHz, 单通道, 16 - bit");
		cboWaveInOpt.AddItem(L"22.05 kHz, 双通道, 8 - bit");
		cboWaveInOpt.AddItem(L"22.05 kHz, 双通道, 16 - bit");
		cboWaveInOpt.AddItem(L"44.1 kHz, 单通道, 8 - bit");
		cboWaveInOpt.AddItem(L"44.1 kHz, 单通道, 16 - bit");
		cboWaveInOpt.AddItem(L"44.1 kHz, 双通道, 8 - bit");
		cboWaveInOpt.AddItem(L"44.1 kHz, 双通道, 16 - bit");
		cboWaveInOpt.ListIndexSet(1);
	}
	//添加网络信息
	form1.Control(ID_cboIPs).AddItem(L"127.0.0.1");
	form1.Control(ID_cboIPs).AddItem(L"172.18.217.77");
	form1.Control(ID_cboIPs).AddItem(L"192.168.43.34");
	form1.Control(ID_cboIPs).AddItem(L"172.17.50.9");
	form1.Control(ID_cboIPs).ListIndexSet(1);
	form1.Control(ID_txtPort).TextSet(Str(4444));

	//socket 的初始化
	m_sSocket.OnCreate(form1.hWnd());
	HM.Dispose();
	form1.Control(ID_cmdStopCall).EnabledSet(false);

	//初始化音频设备
	testrecord.WaveInit(form1.hWnd(), form1.Control(ID_cboWaveInDevs).ListIndex() - 1);

}
void form1_Record(int lp, int wp)
{
	//============================================
	//可能需要用到线程锁，此处仅仅记录一下吧
	//创建线程锁并加锁

	//==============================================
	//中间进行用户想要进行的操作内容
	SSounds sounds;
	sounds.iReceiveID = form1.Control(ID_cboFriendsID).TextInt();
	CopyMemory(sounds.sSoundData, testrecord.GetWaveInAddr(), MAX_BUFFER_SIZE);
	m_sSocket.OnSend((char*)&sounds, sizeof(SSounds), eMsgSounds);

	//=================================
	//清除缓冲区内容
	testrecord.FreeRecordBuffer();
	//此处线程锁解锁

}
void form1_NetWorkEvent(int iSocket, int iEvent, int iError)
{
	m_sSocket.SocketEventsGenerator(iSocket, iEvent, iError);

	switch (iEvent)
	{
	case FD_WRITE:		//连接成功
		form1.TextSet(L"成功和服务器建立连接...");
		m_fIsConnect = true;
		break;
	case FD_READ:		//收到来信
	{
		char* sData;
		MSGHEAD msgHead;
		if (!m_sSocket.OnReceive((char**)&sData, &msgHead, iSocket))
			return;
		switch (msgHead.dwcmdID)
		{
		case eMsgClientIDChange:
		{
			//可以动态的增加或者减少用户的数量
			SClientIDChange* clientchange = (SClientIDChange*)sData;
			if (clientchange->fAddorSub)
			{
				form1.Control(ID_cboFriendsID).AddItem(Str(clientchange->iChangeID));
			}
			else
			{
				int i;
				for (i = 1; i <= form1.Control(ID_cboFriendsID).ListCount(); i++)
				{
					if ((int)Val(form1.Control(ID_cboFriendsID).ItemText(i)) == clientchange->iChangeID)
					{
						form1.Control(ID_cboFriendsID).RemoveItem(i);
						break;
					}
				}
			}
		}
			break;
		case eMsgClientID:
			form1.Control(ID_txtMyID).TextSet(Str((int)(*sData)));
			break;
		case eMsgCallRequest:
		{
			SRequestBack sback;
			sback.iReceiveID = ((SCallRequest*)sData)->iLaunchID;
			if (MsgBox(StrAppend(L"您收到了一位ID为", Str(sback.iReceiveID), L"的用户的呼叫，是否接听？"),
				pApp->Path(), mb_YesNo, mb_IconQuestion) == idYes)
			{
				//或者就是别人主动拨打我的号码的时候我要初始化音频设备内容

				form1.TextSet(L"正在通话...");
				testrecord.StartRecord();
				sback.iLaunchID = form1.Control(ID_txtMyID).TextInt();
				sback.fRequestBack = true;
				form1.Control(ID_cboFriendsID).EnabledSet(false);	//设置为只读，禁止改变
				form1.Control(ID_cmdCall).EnabledSet(false);		//禁止在拨打电话的时候继续拨打电话
				form1.Control(ID_cmdStopCall).EnabledSet(true);
			}
			else
			{
				sback.fRequestBack = false;
				form1.Control(ID_cboFriendsID).EnabledSet(true);	
				form1.Control(ID_cmdCall).EnabledSet(true);		//禁止在拨打电话的时候继续拨打电话
				form1.Control(ID_cmdStopCall).EnabledSet(false);
			}
			m_sSocket.OnSend((char*)&sback, sizeof(SRequestBack), eMsgRequestBack);
		}
			break;
		case eMsgRequestBack:
			if (((SRequestBack*)sData)->fRequestBack)
			{
				form1.TextSet(L"正在通话...");
				testrecord.StartRecord();
				form1.Control(ID_cboFriendsID).EnabledSet(false);	//设置为只读，禁止改变
				form1.Control(ID_cmdCall).EnabledSet(false);		//禁止在拨打电话的时候继续拨打电话
				form1.Control(ID_cmdStopCall).EnabledSet(true);
			}
			else
			{
				form1.TextSet(L"对方已拒绝...");
				MsgBox(L"对方已拒绝您的通话请求，请稍后再拨...");
				form1.Control(ID_cboFriendsID).EnabledSet(true);	//设置为只读，禁止改变
				form1.Control(ID_cmdCall).EnabledSet(true);		//禁止在拨打电话的时候继续拨打电话
				form1.Control(ID_cmdStopCall).EnabledSet(false);
			}
			break;
		case eMsgSounds:
			//播放声音流数据并且清空输出缓冲区
			testrecord.StartPlay(((SSounds*)sData)->sSoundData, MAX_BUFFER_SIZE);
			testrecord.FreePlayBuffer();
			break;
		case eMsgHangUp:
			form1.TextSet(L"另一位用户挂断了语音通话...");
			testrecord.StopRecord();
			testrecord.StopPlay();
			testrecord.FreePlayBuffer();
			testrecord.FreeRecordBuffer();
			form1.Control(ID_cmdCall).EnabledSet(true);
			form1.Control(ID_cmdStopCall).EnabledSet(false);
			form1.Control(ID_cboFriendsID).EnabledSet(true);
			break;
		default:
			break;
		}
	}
		break;
	case FD_CLOSE:		//收到来信
		form1.TextSet(L"通话中断，请稍后再拨...");
		testrecord.StopRecord();
		testrecord.StopPlay();
		testrecord.FreePlayBuffer();
		testrecord.FreeRecordBuffer();
		form1.Control(ID_cboFriendsID).ListClear();
		form1.Control(ID_cboFriendsID).EnabledSet(true);
		form1.Control(ID_cmdCall).EnabledSet(true);
		form1.Control(ID_cmdStopCall).EnabledSet(false);
		m_fIsConnect = false;
		m_sSocket.OnClose();
		break;
	default:
		break;
	}

}
void cmdCall_Click()
{
	//然后要设置对应的用户 id
	if (!m_fIsConnect)
	{
		MsgBox(L"请先与服务器建立连接后重试！", L"未连接服务器");
		return;
	}
	if (*form1.Control(ID_txtMyID).Text() == L'\0')
	{
		MsgBox(L"请填写您的通话ID，本ID主要用于服务器标识用户。", L"无本地ID");
		return;
	}
	if (*form1.Control(ID_cboFriendsID).Text() == L'\0')
	{
		MsgBox(L"请填写您要通话朋友的通话ID，本ID主要用于服务器查找朋友信息。", L"无朋友ID");
		return;
	}
	//testrecord.WaveInit(form1.hWnd(), form1.Control(ID_cboWaveInDevs).ListIndex() - 1);

	form1.Control(ID_cboFriendsID).EnabledSet(false);	//设置为只读，禁止改变
	form1.Control(ID_cmdCall).EnabledSet(false);		//禁止在拨打电话的时候继续拨打电话
	form1.Control(ID_cmdStopCall).EnabledSet(true);

	//发送请求数据内容
	SCallRequest call;
	call.iLaunchID = form1.Control(ID_txtMyID).TextInt();
	call.iReceiveID = form1.Control(ID_cboFriendsID).TextInt();
	m_sSocket.OnSend((char*)&call, sizeof(SCallRequest), eMsgCallRequest);
}
void cmdStopCall_Click()
{
	m_sSocket.OnSend(NULL, 0, eMsgHangUp);		//发送挂断消息
	form1.TextSet(L"已挂断语音通话...");
	testrecord.StopRecord();
	form1.Control(ID_cmdCall).EnabledSet(true);
	form1.Control(ID_cmdStopCall).EnabledSet(false);
	form1.Control(ID_cboFriendsID).EnabledSet(true);
}
void cmdConnect_Click()
{
	if(m_sSocket.OnConnect(form1.Control(ID_cboIPs).Text(), form1.Control(ID_txtPort).TextInt()))
		form1.TextSet(L"正在连接服务器...");
	else
		form1.TextSet(L"服务器连接失败，请重试...");
}
void cboWaveInDevs_SelChange()
{
	//更新设备内容
	if (!form1.Control(ID_cmdCall).Enabled())
	{
		//如果正在通话，先停止录音，改变设备后重新录音
		testrecord.StopRecord();
		testrecord.WaveInit(form1.hWnd(), form1.Control(ID_cboWaveInDevs).ListIndex() - 1);
		testrecord.StartRecord();
	}
	else
		testrecord.WaveInit(form1.hWnd(), form1.Control(ID_cboWaveInDevs).ListIndex() - 1);
}
int main()
{
	form1.EventAdd(0, eForm_Load, form1_Load);
	form1.EventAdd(0, eRecordNotify, form1_Record);
	form1.EventAdd(0, eNetWorkEvent, form1_NetWorkEvent);

	form1.EventAdd(ID_cboWaveInDevs, eComboBox_SelChange, cboWaveInDevs_SelChange);
	form1.EventAdd(ID_cmdConnect, eCommandButton_Click, cmdConnect_Click);
	form1.EventAdd(ID_cmdCall, eCommandButton_Click, cmdCall_Click);
	form1.EventAdd(ID_cmdStopCall, eCommandButton_Click, cmdStopCall_Click);

	form1.IconSet(IDI_ICON1);
	form1.Show();
	return 0;
}








