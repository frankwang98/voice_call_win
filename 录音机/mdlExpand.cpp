//////////////////////////////////////////////////////////////////////////
// mdlExpand.cpp
//扩展函数文件，主要用于自己日后的使用过程的需求开发
//时间：2020/12/10
//////////////////////////////////////////////////////////////////////////

#include "mdlExpand.h"

//////////////////////////////////////////////////////////////////////////
//部分变量定义：
RECORD_CRUSH_FUN m_pFunCrushHandler = NULL;		//程序出错后的运行程序的地址
TCHAR m_sDmpFileAddr[1024] = {0};				//要保存dmp文件的路径
bool m_fAutoRestart = false;					//是否在出错崩溃后自动重新启动程序


//////////////////////////////////////////////////////////////////////////
//将string转换成tstring  
tstring stringTotstring(string str)
{
	tstring result;
	//获取缓冲区大小，并申请空间，缓冲区大小按字符计算  
	int len = MultiByteToWideChar(CP_ACP, 0, str.c_str(), str.size(), NULL, 0);
	TCHAR* buffer = new TCHAR[len + 1];
	//多字节编码转换成宽字节编码  
	MultiByteToWideChar(CP_ACP, 0, str.c_str(), str.size(), buffer, len);
	buffer[len] = '\0';             //添加字符串结尾  
	//删除缓冲区并返回值  
	result.append(buffer);
	delete[] buffer;
	return result;
}

//将tstring转换成string  
string tstringTostring(tstring wstr)
{
	string result;
	//获取缓冲区大小，并申请空间，缓冲区大小事按字节计算的  
	int len = WideCharToMultiByte(CP_ACP, 0, wstr.c_str(), wstr.size(), NULL, 0, NULL, NULL);
	char* buffer = new char[len + 1];
	//宽字节编码转换成多字节编码  
	WideCharToMultiByte(CP_ACP, 0, wstr.c_str(), wstr.size(), buffer, len, NULL, NULL);
	buffer[len] = '\0';
	//删除缓冲区并返回值  
	result.append(buffer);
	delete[] buffer;
	return result;
}

int UnicodeToGB2312(const tstring& sUnicode, tstring& sGB2312)
{
	int n = WideCharToMultiByte(CP_ACP, 0, sUnicode.c_str(), -1, 0, 0, 0, 0);
	sGB2312.resize(n);
	WideCharToMultiByte(CP_ACP, 0, sUnicode.c_str(), -1, (LPSTR)sGB2312.c_str(), n, 0, 0);
	return n;
}

tstring UnicodeToGB2312(const tstring& sUnicode)
{
	//重载版本
	tstring sTextGb2312 = TEXT("");
	UnicodeToGB2312(sUnicode, sTextGb2312);
	return sTextGb2312;
}

int GB2312ToUnicode(const tstring& sGB2312, tstring& sUnicode)
{
	int n = MultiByteToWideChar(CP_ACP, 0, (LPCCH)sGB2312.c_str(), -1, NULL, 0);
	sUnicode.resize(n);
	MultiByteToWideChar(CP_ACP, 0, (LPCCH)sGB2312.c_str(), -1, (LPWSTR)sUnicode.c_str(), sUnicode.length());
	return n;
}

tstring GB2312ToUnicode(const tstring& sGB2312)
{
	//重载版本
	tstring sUnicode = TEXT("");
	UnicodeToGB2312(sGB2312, sUnicode);
	return sUnicode;
}

int UnicodeToUTF8(const tstring& src, tstring& result)
{
	int n = WideCharToMultiByte(CP_UTF8, 0, src.c_str(), -1, 0, 0, 0, 0);
	result.resize(n);
	WideCharToMultiByte(CP_UTF8, 0, src.c_str(), -1, (char*)result.c_str(), result.length(), 0, 0);
	return n;
}

tstring UnicodeToUTF8(const tstring& sUnicode)
{
	//重载版本
	tstring sUTF8 = TEXT("");
	UnicodeToUTF8(sUnicode, sUTF8);
	return sUTF8;
}

int UTF8ToUnicode(const tstring& src, tstring& result)
{
	int n = MultiByteToWideChar(CP_UTF8, 0, (LPCCH)src.c_str(), -1, NULL, 0);
	result.resize(n);
	MultiByteToWideChar(CP_UTF8, 0, (LPCCH)src.c_str(), -1, (LPWSTR)result.c_str(), result.length());
	return n;
}

tstring UTF8ToUnicode(const tstring& sUTF8)
{
	//重载版本
	tstring sUnicode = TEXT("");
	UTF8ToUnicode(sUTF8, sUnicode);
	return sUnicode;
}

tstring tstringToHexStr(const tstring& str) 
{
	//首先编码转换
	string sInput = tstringTostring(str);

	// 根据默认编码获取字节数组
	string hexString = "0123456789abcdef";
	string sb;
	// 将字节数组中每个字节拆解成2位16进制整数
	for (int i = 0; i < (int)sInput.length(); i++) {
		sb += hexString.at((sInput[i] & 0xf0) >> 4);
		sb += hexString.at((sInput[i] & 0x0f) >> 0);
	}
	//返回转换后的 tstring 对象
	return stringTotstring(sb);
}

tstring HexStrTotstring(const tstring& hexStr)
{
	//先进行输入内容的转换
	string sInput = tstringTostring(hexStr);

	string ret;
	string hexString = "0123456789abcdef";
	// 将每2位16进制整数组装成一个字节
	for (int i = 0; i < (int)sInput.length(); i += 2)
		ret += BYTE(hexString.find(sInput.at(i)) << 4 | hexString.find(sInput.at(i + 1)));

	return stringTotstring(ret);
}

void HexStrToBytes(const tstring& shex, BYTE* bytes)
{
	//首先进行编码转换
	string hex = tstringTostring(shex);

	int bytelen = hex.length() / 2;
	string strByte;
	unsigned int n;
	for (int i = 0; i < bytelen; i++)
	{
		strByte = hex.substr(i * 2, 2);
		sscanf((char*)strByte.c_str(), "%x", &n);
		bytes[i] = n;
	}
}

tstring BytesToHexStr(const BYTE* bytes, const int length)
{
	if (bytes == NULL) {
		return L"";
	}
	string buff;
	const int len = length;
	for (int j = 0; j < len; j++) {
		/*if ((bytes[j] & 0xff) < 16) {
			buff.append("0");
		}*/
		int high = bytes[j] / 16, low = bytes[j] % 16;
		buff += (high < 10) ? ('0' + high) : ('a' + high - 10);
		buff += (low < 10) ? ('0' + low) : ('a' + low - 10);
	}
	return stringTotstring(buff);
}

//判断是否进程运行
//param1:进程名
bool IsProcessRun(tstring sProcessName)
{
	//创建进程快照
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (INVALID_HANDLE_VALUE == hSnapshot)
		return false;

	PROCESSENTRY32 currentProcess = { sizeof(currentProcess) };

	BOOL bMore = Process32First(hSnapshot, &currentProcess);		//获取第一个进程信息
	bool fIsThisExe = false;	//是否是本进程
	while (bMore)
	{
		if (_tcscmp(currentProcess.szExeFile, sProcessName.c_str()) == 0)			//比较是否存在此进程
		{
			if (fIsThisExe)
			{
				CloseHandle(hSnapshot);		//清除hProcess句柄
				return true;
			}
			else
			{
				fIsThisExe = true;		//否则先设置标志变量，下一次再自动退出
			}
		}
		bMore = Process32Next(hSnapshot, &currentProcess);			//遍历下一个
	}

	CloseHandle(hSnapshot);
	return false;
}

//检查是否有节日，此处可以在内部编辑具体的内容
//如果有返回对应的节日，如果没有返回空字符串
tstring CheckForFestival()
{
	//此处可以添加小功能，监测时间
	//可以说是一个彩蛋吧
	tstring sTime = Now();
	tstring sTemp = sTime.substr(5, 5);
	if (sTemp == TEXT("05-01"))
		return TEXT("劳动节");
	else if (sTemp == TEXT("06-01"))
		return TEXT("儿童节");
	else if (sTemp == TEXT("10-01"))
		return TEXT("国庆节");
	else if (sTemp == TEXT("11-11"))
		return TEXT("光棍节");
	else if (sTemp == TEXT("11-26"))
		return TEXT("感恩节");
	else if (sTemp == TEXT("12-24"))
		return TEXT("平安夜");
	else if (sTemp == TEXT("06-01"))
		return TEXT("圣诞节");
	else if (sTemp == TEXT("01-01"))
		return TEXT("元旦");
	else if (sTemp == TEXT("02-14"))
		return TEXT("情人节");
	else if (sTemp == TEXT("04-01"))
		return TEXT("愚人节");
	else if (sTemp == TEXT("05-01"))
		return TEXT("劳动节");
	else if (sTemp == TEXT("03-07"))
		return TEXT("我的生日");
	else if (sTemp == TEXT("01-08"))
		return TEXT("好同学陈毯的生日");	
	else
	{
		if (sTime[6] == TEXT('5') && Val(sTime.substr(8, 2).c_str()) >= 8 && Val(sTime.substr(8, 2).c_str()) <= 14 && sTime[11] == TEXT('0'))
			return TEXT("母亲节");
		else if (sTime[6] == TEXT('6') && Val(sTime.substr(8, 2).c_str()) >= 15 && Val(sTime.substr(8, 2).c_str()) <= 21 && sTime[11] == TEXT('0'))
			return TEXT("父亲节");
		else
			return TEXT("");
	}
}

//工具函数
double tstringSwitchdouble(tstring sSwitch)
{
	double DoubleReturn = Val(sSwitch.c_str());

	switch (sSwitch[0])
	{
	case 'A':
		DoubleReturn = 10;
		break;
	case 'B':
		DoubleReturn = 11;
		break;
	case 'C':
		DoubleReturn = 12;
		break;
	case 'D':
		DoubleReturn = 13;
		break;
	case 'E':
		DoubleReturn = 14;
		break;
	case 'F':
		DoubleReturn = 15;
		break;
	case 'a':
		DoubleReturn = 10;
		break;
	case 'b':
		DoubleReturn = 11;
		break;
	case 'c':
		DoubleReturn = 12;
		break;
	case 'd':
		DoubleReturn = 13;
		break;
	case 'e':
		DoubleReturn = 14;
		break;
	case 'f':
		DoubleReturn = 15;
		break;
	default:
		break;
	}
	return DoubleReturn;
}
tstring BOHToDec(int iStyle, tstring sBOH)
{
	//n进制转换为十进制
	//iStyle为几，默认转换就是多少
	//转换成功，耶

	int i = 0;
	tstring sSubstr = TEXT("");
	double sDecResult = 0;
	int iPos = 0;

	if (sBOH == TEXT(""))
	{
		return TEXT("");
	}

	iPos = sBOH.find(TEXT("."));
	if (iPos != string::npos)
	{
		//先切割前面的内容，然后作为数学方式相加
		sSubstr = sBOH.substr(0, iPos);
		for (i = 0; i <= iPos; i++)
		{
			sDecResult += tstringSwitchdouble(sSubstr.substr(i, 1)) * pow(iStyle, (double)iPos - i - 1);	//害，这里的iPos是小数点的位置
		}
		//后面小数部分继续加 
		sSubstr = sBOH.substr(iPos + 1);
		for (i = 0; i < (int)sSubstr.length(); i++)
		{
			sDecResult += tstringSwitchdouble(sSubstr.substr(i, 1)) * pow(iStyle, -(double)(i + 1));
		}
	}
	else
	{
		//直接转换，不考虑什么小数点的问题，多好
		iPos = sBOH.length() - 1;
		for (i = 0; i <= iPos; i++)
		{
			sDecResult += tstringSwitchdouble(sBOH.substr(i, 1)) * pow(iStyle, (double)iPos - i);
		}
	}

	return Str(sDecResult);

}

//进制转换函数
tstring DecToBit(tstring sDec)
{
	//十进制转换为二进制，此处支持小数转换
	//还是挺流畅的，这里没有遇到什么大问题

	tstring sTemp = TEXT("");
	tstring sBitResult = TEXT("");
	double iDouble = 0;
	int iInt = 0;
	int i = 0;

	//////////////////////////////////////////////////////////////////////////
	//整数部分的二进制转换，成功了，没有难度，嘻嘻

	iInt = (int)Val(sDec.c_str());	//这里还要照顾一下这个数值的 最大值，否则就转换不了了

	for (i = 0;; i++)
	{
		if (iInt == 0)	//这里是如果最后的数为0，就证明计算结束了
			break;

		sTemp = Str(iInt % 2);
		sBitResult = sTemp + sBitResult;
		iInt = iInt / 2;
	}
	//////////////////////////////////////////////////////////////////////////
	//下面是小数部分的转换,转换成功，嘻嘻
	int iPos = sDec.find(TEXT("."));
	if (iPos != string::npos && sDec.length() - 1 != iPos)
	{
		iDouble = Val(sDec.c_str()) - (int)Val(sDec.c_str());
		sBitResult = sBitResult + TEXT(".");
		for (i = 0; i < 28; i++)
		{
			if (iDouble == 0)	//转换结束标志就是最终结果为0
				break;
			iDouble = iDouble * 2;
			sTemp = Str((int)(iDouble));
			sBitResult = sBitResult + sTemp;
			iDouble = iDouble - (int)iDouble;
		}
	}

	return sBitResult;
}

tstring BitToOct(tstring sDec)
{
	//十进制转换为八进制
	//此处和计算的思路一样，提取三位，对应，然后写出结果

	tstring sOctResult = TEXT("");
	tstring sSubstr = TEXT("");
	int i = 0;

	//这里进行三位三位的比对转换
	for (i = 0; i < (int)sDec.length(); i = i + 3)
	{
		if (sDec[i] == TEXT('.'))
		{
			sOctResult += TEXT(".");	//添加小数点功能
			i++;					//这里为了跳过小数点需要后移一位
		}
		sSubstr = sDec.substr(i, 3);
		if (sSubstr == TEXT("000"))
			sOctResult += TEXT("0");
		else if (sSubstr == TEXT("001"))
			sOctResult += TEXT("1");
		else if (sSubstr == TEXT("010"))
			sOctResult += TEXT("2");
		else if (sSubstr == TEXT("011"))
			sOctResult += TEXT("3");
		else if (sSubstr == TEXT("100"))
			sOctResult += TEXT("4");
		else if (sSubstr == TEXT("101"))
			sOctResult += TEXT("5");
		else if (sSubstr == TEXT("110"))
			sOctResult += TEXT("6");
		else if (sSubstr == TEXT("111"))
			sOctResult += TEXT("7");
		else
			sOctResult += TEXT("");
	}
	return sOctResult;
}

tstring BitToHex(tstring sDec)
{
	//十进制转换为十六进制
	//其实和转换为八进制的差不多，只不过位数多了一位

	tstring sHexResult = TEXT("");
	tstring sSubstr = TEXT("");
	int i = 0;

	//这里进行四位四位的比对转换
	for (i = 0; i < (int)sDec.length(); i = i + 4)
	{
		if (sDec[i] == TEXT('.'))
		{
			sHexResult += TEXT(".");	//添加小数点功能
			i++;					//这里为了跳过小数点需要后移一位
		}
		sSubstr = sDec.substr(i, 4);
		if (sSubstr == TEXT("0000"))
			sHexResult += TEXT("0");
		else if (sSubstr == TEXT("0001"))
			sHexResult += TEXT("1");
		else if (sSubstr == TEXT("0010"))
			sHexResult += TEXT("2");
		else if (sSubstr == TEXT("0011"))
			sHexResult += TEXT("3");
		else if (sSubstr == TEXT("0100"))
			sHexResult += TEXT("4");
		else if (sSubstr == TEXT("0101"))
			sHexResult += TEXT("5");
		else if (sSubstr == TEXT("0110"))
			sHexResult += TEXT("6");
		else if (sSubstr == TEXT("0111"))
			sHexResult += TEXT("7");
		else if (sSubstr == TEXT("1000"))
			sHexResult += TEXT("8");
		else if (sSubstr == TEXT("1001"))
			sHexResult += TEXT("9");
		else if (sSubstr == TEXT("1010"))
			sHexResult += TEXT("A");
		else if (sSubstr == TEXT("1011"))
			sHexResult += TEXT("B");
		else if (sSubstr == TEXT("1100"))
			sHexResult += TEXT("C");
		else if (sSubstr == TEXT("1101"))
			sHexResult += TEXT("D");
		else if (sSubstr == TEXT("1110"))
			sHexResult += TEXT("E");
		else if (sSubstr == TEXT("1111"))
			sHexResult += TEXT("F");
		else
			sHexResult += TEXT("");
	}
	return sHexResult;
}

tstring BitToDec(tstring sBit)
{
	return BOHToDec(2, sBit);
}

tstring OctToDec(tstring sOct)
{
	return BOHToDec(8, sOct);
}

tstring HexToDec(tstring sHex)
{
	return BOHToDec(16, sHex);
}

void AutoRecdCrashError(RECORD_CRUSH_FUN pFunCrushReport /*= NULL*/, LPCTSTR sDmpAddr /*= NULL*/, bool fAutoRestart /*= false*/)
{
	SetUnhandledExceptionFilter(ExceptionFilter);
	m_pFunCrushHandler = pFunCrushReport;
	if(m_sDmpFileAddr != TEXT(""))
		_tcscpy(m_sDmpFileAddr, sDmpAddr);
	m_fAutoRestart = fAutoRestart;
}

//产生错误信息的 dmp 文件，便于后面的调试
int GenerateMiniDump(PEXCEPTION_POINTERS pExceptionPointers)
{
	// 定义函数指针
	typedef BOOL(WINAPI* MiniDumpWriteDumpT)(
		HANDLE,
		DWORD,
		HANDLE,
		MINIDUMP_TYPE,
		PMINIDUMP_EXCEPTION_INFORMATION,
		PMINIDUMP_USER_STREAM_INFORMATION,
		PMINIDUMP_CALLBACK_INFORMATION
		);
	// 从 "DbgHelp.dll" 库中获取 "MiniDumpWriteDump" 函数
	MiniDumpWriteDumpT pfnMiniDumpWriteDump = NULL;
	HMODULE hDbgHelp = LoadLibrary(L"DbgHelp.dll");
	if (NULL == hDbgHelp)
	{
		return EXCEPTION_CONTINUE_EXECUTION;
	}
	pfnMiniDumpWriteDump = (MiniDumpWriteDumpT)GetProcAddress(hDbgHelp, "MiniDumpWriteDump");

	if (NULL == pfnMiniDumpWriteDump)
	{
		FreeLibrary(hDbgHelp);
		return EXCEPTION_CONTINUE_EXECUTION;
	}
	// 创建 dmp 文件件
	tstring szFileName = L"";
	szFileName.resize(MAX_PATH);
	TCHAR* szVersion = (LPTSTR)TEXT("Dump");
	SYSTEMTIME stLocalTime;
	GetLocalTime(&stLocalTime);
	wsprintf((LPTSTR)szFileName.c_str(), TEXT("%s-%04d%02d%02d-%02d%02d%02d.dmp"),
		szVersion, stLocalTime.wYear, stLocalTime.wMonth, stLocalTime.wDay,
		stLocalTime.wHour, stLocalTime.wMinute, stLocalTime.wSecond);
	//如果此处用户指定了保存位置，那么就将出错信息路径加上保存的位置的内容
	if (m_sDmpFileAddr != TEXT(""))
		szFileName = m_sDmpFileAddr + szFileName;
	HANDLE hDumpFile = CreateFile(szFileName.c_str(), GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_WRITE | FILE_SHARE_READ, 0, CREATE_ALWAYS, 0, 0);
	if (INVALID_HANDLE_VALUE == hDumpFile)
	{
		FreeLibrary(hDbgHelp);
		return EXCEPTION_CONTINUE_EXECUTION;
	}
	// 写入 dmp 文件
	MINIDUMP_EXCEPTION_INFORMATION expParam;
	expParam.ThreadId = GetCurrentThreadId();
	expParam.ExceptionPointers = pExceptionPointers;
	expParam.ClientPointers = FALSE;
	pfnMiniDumpWriteDump(GetCurrentProcess(), GetCurrentProcessId(),
		hDumpFile, MiniDumpWithDataSegs, (pExceptionPointers ? &expParam : NULL), NULL, NULL);
	// 释放文件
	CloseHandle(hDumpFile);
	FreeLibrary(hDbgHelp);

	//调用用户自定义的出错处理函数
	tstring sParam = TEXT("");		//此处用来记录用户传递过来的参数
	if (m_pFunCrushHandler)
		sParam = m_pFunCrushHandler();

	//判断是否重新启动，如果重新启动，那么此处重新启动程序
	if (m_fAutoRestart)
	{
		TCHAR m_path[1024] = { 0 };
		GetModuleFileName(0, m_path, sizeof(m_path));
		ShellExecute(0, TEXT("open"), m_path, sParam.c_str(), 0, SW_RESTORE);
	}

	return EXCEPTION_EXECUTE_HANDLER;
}

LONG WINAPI ExceptionFilter(LPEXCEPTION_POINTERS lpExceptionInfo)
{
	// 这里做一些异常的过滤或提示
	if (IsDebuggerPresent())
	{
		return EXCEPTION_CONTINUE_SEARCH;
	}
	return GenerateMiniDump(lpExceptionInfo);
}

//////////////////////////////////////////////////////////////////////////
// 
/// </summary>
/// <param name="source"></param>
/// <returns></returns>

int CCharEncode::UnicodeToUTF8(tstring& source, tstring& dest)
{

	string sUnicode = tstringTostring(source);
	int sourcesize = sUnicode.size();
	string src;
	unsigned char pout[8];
	int num = 0;
	int destnum = 0;
	for (int index = 0; index < sourcesize;)
	{
		memset(pout, 0, 8);
		src = sUnicode.substr(index, 6);

		dest.resize(dest.length() + 1);		//每次都要重新增加一个空间，
									//如果不是编码，直接将内容填进去
		if (isUnicode(src) == 1)
		{
			string hexsrc = sUnicode.substr(index + 2, 4);
			num = xstrtoshortint(hexsrc.c_str());			//获得该字节的 int 数据
			dest[dest.length()-1] = num;
			index += 6;
			//sourcesize = source.size();
// 			string hexsrc = source.substr(index + 2, 4);
// 			int num = enc_unicode_to_utf8_one(xstrtoshortint(hexsrc.c_str()), pout, 8);
// 			ReplaceStr(dest, src.c_str(), (char*)pout);
// 			index += 3;
// 			sourcesize = source.size();
		}
		else
		{
			dest[dest.length() - 1] = sUnicode[index];	//直接将内容填进去
			index++;
		}
	}
	return 0;
}


tstring CCharEncode::UnicodeToUTF8(tstring& source)
{
	tstring sDest = L"";
	UnicodeToUTF8(source, sDest);
	return sDest;
}

int CCharEncode::UTF8ToUnicode(tstring& source, tstring& dest)
{
	

	return 0;
}

int CCharEncode::enc_unicode_to_utf8_one(unsigned long unic, unsigned char* pOutput, int outSize)
{
	assert(pOutput != NULL);
	assert(outSize >= 6);


	if (unic <= 0x0000007F)
	{
		// * U-00000000 - U-0000007F:  0xxxxxxx
		*pOutput = (unic & 0x7F);
		return 1;
	}
	else if (unic >= 0x00000080 && unic <= 0x000007FF)
	{
		// * U-00000080 - U-000007FF:  110xxxxx 10xxxxxx
		*(pOutput + 1) = (unic & 0x3F) | 0x80;
		*pOutput = ((unic >> 6) & 0x1F) | 0xC0;
		return 2;
	}
	else if (unic >= 0x00000800 && unic <= 0x0000FFFF)
	{
		// * U-00000800 - U-0000FFFF:  1110xxxx 10xxxxxx 10xxxxxx
		*(pOutput + 2) = (unic & 0x3F) | 0x80;
		*(pOutput + 1) = ((unic >> 6) & 0x3F) | 0x80;
		*pOutput = ((unic >> 12) & 0x0F) | 0xE0;
		return 3;
	}
	else if (unic >= 0x00010000 && unic <= 0x001FFFFF)
	{
		// * U-00010000 - U-001FFFFF:  11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
		*(pOutput + 3) = (unic & 0x3F) | 0x80;
		*(pOutput + 2) = ((unic >> 6) & 0x3F) | 0x80;
		*(pOutput + 1) = ((unic >> 12) & 0x3F) | 0x80;
		*pOutput = ((unic >> 18) & 0x07) | 0xF0;
		return 4;
	}
	else if (unic >= 0x00200000 && unic <= 0x03FFFFFF)
	{
		// * U-00200000 - U-03FFFFFF:  111110xx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx
		*(pOutput + 4) = (unic & 0x3F) | 0x80;
		*(pOutput + 3) = ((unic >> 6) & 0x3F) | 0x80;
		*(pOutput + 2) = ((unic >> 12) & 0x3F) | 0x80;
		*(pOutput + 1) = ((unic >> 18) & 0x3F) | 0x80;
		*pOutput = ((unic >> 24) & 0x03) | 0xF8;
		return 5;
	}
	else if (unic >= 0x04000000 && unic <= 0x7FFFFFFF)
	{
		// * U-04000000 - U-7FFFFFFF:  1111110x 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx
		*(pOutput + 5) = (unic & 0x3F) | 0x80;
		*(pOutput + 4) = ((unic >> 6) & 0x3F) | 0x80;
		*(pOutput + 3) = ((unic >> 12) & 0x3F) | 0x80;
		*(pOutput + 2) = ((unic >> 18) & 0x3F) | 0x80;
		*(pOutput + 1) = ((unic >> 24) & 0x3F) | 0x80;
		*pOutput = ((unic >> 30) & 0x01) | 0xFC;
		return 6;
	}
	return 0;
}


int CCharEncode::isUnicode(const string& src)
{
	if (src.size() != 6)
		return 0;
	if (src.find("\\u", 0) == 0)
	{
		for (int i = 2; i <= 5; i++)
		{
			if (!((src[i] >= 'a' && src[i] <= 'f')
				|| (src[i] >= 'A' && src[i] <= 'F')
				|| (src[i] >= '0' && src[i] <= '9')))
			{
				return 0;
			}
		}
		return 1;
	}
	else
	{
		return 0;
	}
}


unsigned int CCharEncode::xstrtoshortint(const char* str)
{
	int len = strlen(str);
	unsigned int ivalue = 0;
	for (int i = 0; i < len; i++)
	{
		if ((str[i] <= '9' && str[i] >= '0'))
		{
			ivalue = ivalue * 16 + (str[i] - '0'); //16进制 可换其它进制
		}
		else if ((str[i] >= 'a' && str[i] <= 'f'))
		{
			ivalue = ivalue * 16 + (str[i] - 'a') + 10;
		}
		else if ((str[i] >= 'A' && str[i] <= 'F'))
		{
			ivalue = ivalue * 16 + (str[i] - 'A') + 10;
		}
	}
	return ivalue;
}


void CCharEncode::ReplaceStr(string& strContent, const char* strSrc, const char* strDest)
{
	string strCopy(strContent);
	string strSrcCopy(strSrc);


	string::size_type pos = 0;
	string::size_type srclen = strlen(strSrc);
	if ((pos = strCopy.find(strSrcCopy, pos)) != string::npos)
	{
		strContent.replace(pos, srclen, strDest);
	}
}

void CreateFileFromStream(LPCTSTR sFilePath, LPVOID pDataToWrite, int iDataLen)
{
	//首先判断这个文件是否存在，如果存在，直接删除，重新创建一个新的内容覆盖
	WIN32_FIND_DATA fd;
	HANDLE hFileFind = FindFirstFile(sFilePath, &fd);
	//如果已经找到该文件，那么就直接删除该文件
	if (hFileFind != INVALID_HANDLE_VALUE)
		DeleteFile(sFilePath);
	HANDLE  hFile = CreateFile(sFilePath, GENERIC_WRITE, NULL, NULL, CREATE_NEW,
		FILE_ATTRIBUTE_ARCHIVE, NULL);

	//如果创建失败，那么直接返回失败
	if (!hFile)	return;
	//文件创建成功，此处继续进行文件的读写操作，写入文件内容
	DWORD dwSizeWritten = 0;
	WriteFile(hFile, pDataToWrite, iDataLen, &dwSizeWritten, NULL);
	
	CloseHandle(hFile);
}

//////////////////////////////////////////////////////////////////////////
// 字符串编码方式的类
strCoding::strCoding(void)
{
}

strCoding::~strCoding(void)
{
}
void strCoding::Gb2312ToUnicode(WCHAR* pOut, char* gbBuffer)
{
	::MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, gbBuffer, 2, pOut, 1);
	return;
}
void strCoding::UTF_8ToUnicode(WCHAR* pOut, char* pText)
{
	char* uchar = (char*)pOut;

	uchar[1] = ((pText[0] & 0x0F) << 4) + ((pText[1] >> 2) & 0x0F);
	uchar[0] = ((pText[1] & 0x03) << 6) + (pText[2] & 0x3F);

	return;
}

void strCoding::UnicodeToUTF_8(char* pOut, WCHAR* pText)
{
	// 注意 WCHAR高低字的顺序,低字节在前，高字节在后
	char* pchar = (char*)pText;

	pOut[0] = (0xE0 | ((pchar[1] & 0xF0) >> 4));
	pOut[1] = (0x80 | ((pchar[1] & 0x0F) << 2)) + ((pchar[0] & 0xC0) >> 6);
	pOut[2] = (0x80 | (pchar[0] & 0x3F));

	return;
}
void strCoding::UnicodeToGB2312(char* pOut, WCHAR uData)
{
	WideCharToMultiByte(CP_ACP, NULL, &uData, 1, pOut, sizeof(WCHAR), NULL, NULL);
	return;
}

//做为解Url使用
char strCoding::CharToInt(char ch) {
	if (ch >= '0' && ch <= '9')return (char)(ch - '0');
	if (ch >= 'a' && ch <= 'f')return (char)(ch - 'a' + 10);
	if (ch >= 'A' && ch <= 'F')return (char)(ch - 'A' + 10);
	return -1;
}
char strCoding::StrToBin(char* str) {
	char tempWord[2];
	char chn;

	tempWord[0] = CharToInt(str[0]);                         //make the B to 11 -- 00001011
	tempWord[1] = CharToInt(str[1]);                         //make the 0 to 0 -- 00000000

	chn = (tempWord[0] << 4) | tempWord[1];                //to change the BO to 10110000

	return chn;
}


//UTF_8 转gb2312
void strCoding::UTF_8ToGB2312(string& pOut, char* pText, int pLen)
{
	char buf[4];
	char* rst = new char[pLen + (pLen >> 2) + 2];
	memset(buf, 0, 4);
	memset(rst, 0, pLen + (pLen >> 2) + 2);

	int i = 0;
	int j = 0;

	while (i < pLen)
	{
		if (*(pText + i) >= 0)
		{

			rst[j++] = pText[i++];
		}
		else
		{
			WCHAR Wtemp;


			UTF_8ToUnicode(&Wtemp, pText + i);

			UnicodeToGB2312(buf, Wtemp);

			unsigned short int tmp = 0;
			tmp = rst[j] = buf[0];
			tmp = rst[j + 1] = buf[1];
			tmp = rst[j + 2] = buf[2];

			//newBuf[j] = Ctemp[0];
			//newBuf[j + 1] = Ctemp[1];

			i += 3;
			j += 2;
		}

	}
	rst[j] = '\0';
	pOut = rst;
	delete[]rst;
}

//GB2312 转为 UTF-8
void strCoding::GB2312ToUTF_8(string& pOut, char* pText, int pLen)
{
	char buf[4];
	memset(buf, 0, 4);

	pOut.clear();

	int i = 0;
	while (i < pLen)
	{
		//如果是英文直接复制就可以
		if (pText[i] >= 0)
		{
			char asciistr[2] = { 0 };
			asciistr[0] = (pText[i++]);
			pOut.append(asciistr);
		}
		else
		{
			WCHAR pbuffer;
			Gb2312ToUnicode(&pbuffer, pText + i);

			UnicodeToUTF_8(buf, &pbuffer);

			pOut.append(buf);

			i += 2;
		}
	}

	return;
}
//把str编码为网页中的 GB2312 url encode ,英文不变，汉字双字节 如%3D%AE%88
string strCoding::UrlGB2312(char* str)
{
	string dd;
	size_t len = strlen(str);
	for (size_t i = 0; i < len; i++)
	{
		if (isalnum((BYTE)str[i]))
		{
			char tempbuff[2];
			sprintf(tempbuff, "%c", str[i]);
			dd.append(tempbuff);
		}
		else if (isspace((BYTE)str[i]))
		{
			dd.append("+");
		}
		else
		{
			char tempbuff[4];
			sprintf(tempbuff, "%%%X%X", ((BYTE*)str)[i] >> 4, ((BYTE*)str)[i] % 16);
			dd.append(tempbuff);
		}

	}
	return dd;
}

//把str编码为网页中的 UTF-8 url encode ,英文不变，汉字三字节 如%3D%AE%88

string strCoding::UrlUTF8(char* str)
{
	string tt;
	string dd;
	GB2312ToUTF_8(tt, str, (int)strlen(str));

	size_t len = tt.length();
	for (size_t i = 0; i < len; i++)
	{
		if (isalnum((BYTE)tt.at(i)))
		{
			char tempbuff[2] = { 0 };
			sprintf(tempbuff, "%c", (BYTE)tt.at(i));
			dd.append(tempbuff);
		}
		else if (isspace((BYTE)tt.at(i)))
		{
			dd.append("+");
		}
		else
		{
			char tempbuff[4];
			sprintf(tempbuff, "%%%X%X", ((BYTE)tt.at(i)) >> 4, ((BYTE)tt.at(i)) % 16);
			dd.append(tempbuff);
		}

	}
	return dd;
}
//把url GB2312解码
string strCoding::UrlGB2312Decode(string str)
{
	string output = "";
	char tmp[2];
	int i = 0, idx = 0, len = str.length();

	while (i < len) {
		if (str[i] == '%') {
			tmp[0] = str[i + 1];
			tmp[1] = str[i + 2];
			output += StrToBin(tmp);
			i = i + 3;
		}
		else if (str[i] == '+') {
			output += ' ';
			i++;
		}
		else {
			output += str[i];
			i++;
		}
	}

	return output;
}
//把url utf8解码
string strCoding::UrlUTF8Decode(string str)
{
	string output = "";

	string temp = UrlGB2312Decode(str);//

	UTF_8ToGB2312(output, (char*)temp.data(), strlen(temp.data()));

	return output;

}

void CreateFileFromResourse(unsigned int idResourse, LPCTSTR sResourseType, LPCTSTR sFilePath)
{
	HMODULE hThis = GetModuleHandle(NULL);
	HRSRC   hRes = FindResource(hThis, MAKEINTRESOURCE(idResourse), sResourseType);   //第二个参加是资源ID，第三个参数是，添加写的名字
	if (!hThis)	return;		//如果此处不对，建议重新打开，一般不会

	HGLOBAL hGres = LoadResource(hThis, hRes);
	PVOID   pRes = LockResource(hGres);
	DWORD  dwSize = SizeofResource(NULL, hRes);
	//首先判断这个文件是否存在，如果存在，直接删除，重新创建一个新的内容覆盖
	WIN32_FIND_DATA fd;
	HANDLE hFileFind = FindFirstFile(sFilePath, &fd);
	//如果已经找到该文件，那么就直接删除该文件
	if (hFileFind != INVALID_HANDLE_VALUE)
		return;
	HANDLE  hFile = CreateFile(sFilePath, GENERIC_WRITE, NULL, NULL, CREATE_NEW,
		FILE_ATTRIBUTE_ARCHIVE, NULL);

	//如果创建失败，那么直接返回失败
	if (!hFile)	return;
	//文件创建成功，此处继续进行文件的读写操作，写入文件内容
	DWORD dwSizeWritten = 0;
	WriteFile(hFile, pRes, dwSize, &dwSizeWritten, NULL);
	CloseHandle(hFile);
}
