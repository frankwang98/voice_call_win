//////////////////////////////////////////////////////////////////////////
// mdlExpand.h
//扩展函数文件，主要用于自己日后的使用过程的需求开发
//时间：2020/12/10
//////////////////////////////////////////////////////////////////////////

#pragma once
#include "BForm.h"
#include <TlHelp32.h>
#include "DbgHelp.h"
#include <assert.h>
#include <string>

using namespace std;


//此处是自动记录程序出错信息的函数地址，此处可以运行对应的出错函数
typedef LPCTSTR (* RECORD_CRUSH_FUN)(void);

//////////////////////////////////////////////////////////////////////////
//部分变量定义：

//RECORD_CRUSH_FUN g_pFunCrushHandler = NULL;		//程序出错后的运行程序的地址
													//不允许被改变，所以不声明

//////////////////////////////////////////////////////////////////////////
//=========================== 不同编码方式的字符的转换 =========================
//为了方便，统一的转换采用的了 tstring 的类的方式实现，这样子省得进行动态内存的
//释放方面的内容，便于使用
/*
艹，此处的转换分为两种：tstring和string，其中string每一个地方占有一个字节，但是
tstring没一个占有两个字节从而导致此处出现了无数个bug，采用转换函数之后才能够比
较好的进行操作，唉，unicode好是好，但是宽字节真的是挺麻烦的
*/

//===================== tstring 和 string 转换 =============================
//string 转换为 tstring
tstring stringTotstring(string str);

//tstring 转换为 string
string tstringTostring(tstring wstr);

//===========================不同编码方式的转换 =================================
//Unicode 转换为 GB2312
int UnicodeToGB2312(const tstring& sUnicode, tstring& result);
tstring UnicodeToGB2312(const tstring& sUnicode);

//GB2312 转换为 Unicode
int GB2312ToUnicode(const tstring& sGB2312, tstring& result);
tstring GB2312ToUnicode(const tstring& sGB2312);

//Unicode 转换为 UTF8
int UnicodeToUTF8(const tstring& src, tstring& result);
tstring UnicodeToUTF8(const tstring& sUnicode);

//UTF8 转换为 Unicode
int UTF8ToUnicode(const tstring& src, tstring& result);
tstring UTF8ToUnicode(const tstring& sUTF8);

//========================= tstring 和 十六进制 转换===============================
//仅仅支持英文的十六进制编码和解码
//编码成十六进制的内容
tstring tstringToHexStr(const tstring& str);
//从十六进制解码出信息
tstring HexStrTotstring(const tstring& hexStr);

//========================== 字符与字节数组与十六进制转换 ============================

//字符和字节数组的转换
//字节数组转换为文本
tstring BytesToHexStr(const BYTE* bytes, const int length);

//文本转换为字节数组
void HexStrToBytes(const tstring& hex, BYTE* bytes);

//================================ 进制转换 =========================================

//十进制转换为二进制
tstring DecToBit(tstring sDec);

//二进制转换为八进制
tstring BitToOct(tstring sDec);

//二进制转换为十六进制
tstring BitToHex(tstring sDec);

//二进制转换为十进制
tstring BitToDec(tstring sBit);

//八进制转换为十进制
tstring OctToDec(tstring sOct);

//十六进制转换为十进制
tstring HexToDec(tstring sHex);

//================================= 其他函数 ========================================

//判断是否进程运行
//param1:进程名
bool IsProcessRun(tstring sProcessName);

//判断此时是否是节日
tstring CheckForFestival();

//================================ 创建错误信息dmp ====================================

LONG WINAPI ExceptionFilter(LPEXCEPTION_POINTERS lpExceptionInfo);

// 加入崩溃dump文件功能
//pFunCrushReport 表示的是程序出错后的主程序要执行的程序地址
//sDmpAddr 表示要生成的 dmp 文件的位置，如果是空就在目前的运行目录产生
//fAutoRestart 表示是否自动重新启动程序，默认不重新启动
//程序格式：
//void Form1_CrushHandler();
void AutoRecdCrashError(RECORD_CRUSH_FUN pFunCrushReport = NULL, LPCTSTR sDmpAddr = NULL, bool fAutoRestart = false);


//字符编码转换
class CCharEncode
{
public:

	//将 unicode 转换为 utf8，采用的是自定义的转换方式
	//unicode to utf-8
	int UnicodeToUTF8(tstring& source, tstring& dest);
	tstring UnicodeToUTF8(tstring& source);

	//将 utf8 转换为 unicode ，采用的是自定义的转换方式
	// utf-8 to unicode
	int UTF8ToUnicode(tstring& source, tstring& dest);
	
	//字符串忽略大小写字串替换
	void ReplaceStr(string& strContent, const char* strSrc, const char* strDest);

private:
	int enc_unicode_to_utf8_one(unsigned long unic, unsigned char* pOutput, int outSize);
	int isUnicode(const string& src); //the src is unicode or not, total 6 char(0x5e3f).1,yes;2,no
	unsigned int xstrtoshortint(const char* str); //"0x1a3f"->1a3f

};

//通过数据流创建一个文件
void CreateFileFromStream(LPCTSTR sFilePath, LPVOID pDataToWrite, int iDataLen);

//通过资源 id 创建一个文件
void CreateFileFromResourse(unsigned int idResourse, LPCTSTR sResourseType, LPCTSTR sFilePath);

//文本编辑的类
class strCoding
{
public:
	strCoding(void);
	~strCoding(void);

	void UTF_8ToGB2312(string& pOut, char* pText, int pLen);//utf_8转为gb2312
	void GB2312ToUTF_8(string& pOut, char* pText, int pLen); //gb2312 转utf_8
	string UrlGB2312(char* str);                           //urlgb2312编码
	string UrlUTF8(char* str);                             //urlutf8 编码
	string UrlUTF8Decode(string str);                  //urlutf8解码
	string UrlGB2312Decode(string str);                //urlgb2312解码

private:
	void Gb2312ToUnicode(WCHAR* pOut, char* gbBuffer);
	void UTF_8ToUnicode(WCHAR* pOut, char* pText);
	void UnicodeToUTF_8(char* pOut, WCHAR* pText);
	void UnicodeToGB2312(char* pOut, WCHAR uData);
	char CharToInt(char ch);
	char StrToBin(char* str);

};